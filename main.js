const { app, BrowserWindow } = require('electron');
const electronLocalshortcut = require('electron-localshortcut');

if (process.env.ELECTRON_RELOAD) {
  const path = require('path');
  console.log(__dirname);
  require('electron-reload')(__dirname, {
    electron: path.join(__dirname, 'node_modules', '.bin', 'electron'),
  });
}


let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    },
    icon: 'icon.png'
  });

  mainWindow.loadFile('static/index.html');

  mainWindow.on('closed', function () {
    mainWindow = null
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
  electronLocalshortcut.unregisterAll();
  if (process.platform !== 'darwin') app.quit();
})

app.on('activate', function () {
  if (mainWindow === null) createWindow();
})