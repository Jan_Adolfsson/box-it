# box-it

A hitbox editor for 2d game development written by Jan Adolfsson in 2019-2020 as a tool to be used in his upcoming game 13 Ronin.


## Developing

Clone the repo, and install dependencies with `npm install`. You can now run any of the following:

* `electron .` will start the application (if it's been built)
* `npm run build` will build the application
* `npm run autobuild` will build the application and watch for changes to the source code
* `npm start` will concurrently run `npm run autobuild` and `electron .`


## Packaging

TODO: 


## License

TODO:
