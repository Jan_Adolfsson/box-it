import { convToExtPolys } from "./shapes/poly";

const fs = require("fs");

export const BOX_SHEET_NAME = "boxsheet";
export const BOX_SHEET_EXT = "json";

export function convToExtCell(cell) {
    return {
        index: cell.index,
        polygons: convToExtPolys(cell.polys)
    }
}

export function convToExtCells(cells) {
    return cells.map(cell => convToExtCell(cell));
}

export function createNewCell(index) {
    return {
        index: { x: index.x, y: index.y },
        polys: [],
        boxType: "Hurt",
        tags: []
    };
}

export const createBoxSheetPath = (projDir, projName) => {
    return projDir.concat("/", projName, "-", BOX_SHEET_NAME, ".", BOX_SHEET_EXT)
};

export function createNewCells() {
    return [
        {
            "index": { "x": 0, "y": 0 },
            "polys": [
                {
                    "vertexes": [
                        { "x": 50, "y": 50 },
                        { "x": 100, "y": 100 },
                        { "x": 0, "y": 100 },
                        { "x": 50, "y": 50 }],
                    "boxType": "Hurt",
                    "tags": [],
                    "selected": false,
                    "closed": true
                }
            ],
        }
    ];
}

export function deletePolys(cells, filter) {
    cells.forEach(cell => {
        if (cell.polys) {
            for (let i = cell.polys.length - 1; i > -1; --i) {
                const poly = cell.polys[i];
                if (filter(poly)) {
                    cell.polys.splice(i, 1);
                }
            }
        }
    });
}

export function filterPolys(cells, filter) {
    let polysFiltered = [];

    cells.forEach(cell => {
        if (cell.poly) {
            const polys = cell.polys.filter(p => filter(p));
            polysFiltered = polysFiltered.concat(polys);
        }
    })

    return polysFiltered;
}

export const filterPolysAt = (cells, index, filter) => {
    const cell = getCell(cells, index);
    const filtered = cell.polys.filter(filter);
    return filtered;
}

export function getCell(cells, index) {
    return cells.find(b => b.index.x === index.x && b.index.y === index.y);
}

export function getCells(cells, selection) {
    return selection.map(selCell => {
        return cells.find(cell => cell.index.x === selCell.x && cell.index.y === selCell.y);
    })
}

export function getCellsContainingSelectedPolys(cells) {
    return cells.filter(b => {
        if (!b.polys || b.polys.length === 0) {
            return false;
        }

        return b.polys.some(p => p.selected);
    });
}

export const getSelectionAt = (cells, index) => {
    const selection = filterPolysAt(cells, index, p => p.selected);
    return selection;
};

export function offsetCell(cell, offset) {
    cell.index.x += offset.x;
    cell.index.y += offset.y;
}

export function openBoxsheet(path) {
    console.log(`Open boxsheet from: ${path}`);

    const json = fs.readFileSync(path, { encoding: "utf-8" });
    const cells = JSON.parse(json);
    console.log(`Boxsheet read as: ${cells}`);

    return cells;
}

export function saveBoxsheet(path, cells) {
    const json = JSON.stringify(cells);

    console.log(`Save cells ${json} at: ${path}`);

    fs.writeFileSync(path, json, "utf-8");
}

export function selectAll(polys) {
    polys.forEach(poly => {
        poly.selected = true;
    });
}
export function topLeftCell(cells) {
    const topLeft = { x: null, y: null };

    cells.forEach(b => {
        if (!topLeft.x && !topLeft.y) {
            topLeft.x = b.index.x;
            topLeft.y = b.index.y;
        }

        if (b.index.y <= topLeft.y) {
            topLeft.y = b.index.y;

            if (b.index.x < topLeft.x) {
                topLeft.x = b.index.x;
            }
        }
    });

    return topLeft;
}
