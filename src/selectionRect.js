export class SelectionRect {
    constructor(x = 0, y = 0, w = 0, h = 0) {
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;
    }

    empty() {
        return (
            this.x === 0 && this.y === 0 && this.width === 0 && this.height === 0
        );
    }

    clear() {
        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
    }

    toIndexes() {
        const idxs = [];

        for (let y = this.y; y < this.y + this.height; ++y) {
            for (let x = this.x; x < this.x + this.width; ++x) {
                idxs.push({ x, y });
            }
        }

        return idxs;
    }

    set(startIdx, endIdx) {
        this.x = Math.min(startIdx.x, endIdx.x);
        this.y = Math.min(startIdx.y, endIdx.y);
        this.width = Math.abs(endIdx.x - startIdx.x) + 1;
        this.height = Math.abs(endIdx.y - startIdx.y) + 1;
    }

    contains(pt) {
        return this.x <= pt.x &&
            this.y <= pt.y &&
            this.x + this.width >= pt.x &&
            this.y + this.height >= pt.y;
    }
}