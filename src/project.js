const fs = require("fs");
const path = require("path");

export function createNewProject() {
    return {
        "spritesheet": null,
        "spritesheetSize": { "x": 16, "y": 47 },
        "spritesheetPath": null,
        "spriteSize": { "x": 160, "y": 160 },
        "boxPath": null
    };
}

export const projNameFromPath = projPath => {
    const baseName = path.basename(projPath);
    const idx = baseName.lastIndexOf(".");
    if (idx < 0) {
        return baseName;
    }

    return baseName.substring(0, idx);
};

export function saveProject(path, project) {
    const json = JSON.stringify(project);

    console.log(`Save project ${json} at: ${path}`);

    fs.writeFileSync(path, json, "utf-8");
}

export const splitProjPath = (projPath) => {
    const dir = path.dirname(projPath);
    const file = projNameFromPath(projPath);

    return {
        dir,
        file
    }
}
export function openProject(path) {
    console.log(`Open project from: ${path}`);

    const json = fs.readFileSync(path, { encoding: "utf-8" });
    const project = JSON.parse(json);
    console.log(`Project read as: ${json}`);
    return project;
}

