const fs = require("fs");

export function loadTooltips(path) {
    console.log(`Load tooltips from path: ${path}`);

    const json = fs.readFileSync(path, { encoding: "utf-8" });
    const tooltips = JSON.parse(json);

    console.log(`Tooltips loaded as ${json}`);
    return tooltips;
}
