export function AddCellsToSelectionCmd(selection, cells, drawCb) {
    this.selection = selection;
    this.cells = cells;
    this.drawCb = drawCb;

    this.do = function () {
        this.selection.push(...this.cells);
        this.drawCb();
    }

    this.undo = function () {
        this.selection.splice(this.selection.length - this.cells.length);
        this.drawCb();
    }

    this.redo = function () {
        this.selection.push(...this.cells);
        this.drawCb();
    }
}
