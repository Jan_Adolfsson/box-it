export function ReduceCellSelectionCmd(selection, cells, drawCb) {
    this.selection = selection;
    this.cells = cells;
    this.drawCb = drawCb;
    this.cellsRmvd = [];

    this.do = function () {
        const cellsToRmv = this.cells.filter(cell =>
            this.selection.find(i => i.x === cell.x && i.y === cell.y)
        );

        const idxsDesc = cellsToRmv
            .map(cell => this.selection.findIndex(i => i.x === cell.x && i.y === cell.y))
            .sort((a, b) => b - a);

        idxsDesc.forEach(idx => {
            const cellRmvd = this.selection.splice(idx, 1)[0];
            this.cellsRmvd.push({ idx: idx, cell: cellRmvd });
        });

        this.drawCb();
    }

    this.undo = function () {
        this.cellsRmvd.slice()
            .reverse()
            .forEach(rmvd => this.selection.splice(rmvd.idx, 0, rmvd.cell));

        this.drawCb();
    }

    this.redo = function () {
        this.cellsRmvd.forEach(rmvd => this.selection.splice(rmvd.idx, 1));

        this.drawCb();
    }

}
