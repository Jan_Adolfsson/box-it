const { clipboard } = require('electron');

import { getCell } from "../boxsheet.js";
import { convToExtPoly } from "../shapes/poly.js";

export function ExportPolysCmd(cells, index) {
    this.cells = cells;
    this.index = index;

    this.do = function () {
        const cell = getCell(this.cells, this.index);
        const selectedPolys = cell
            .polys
            .filter(poly => poly.selected);

        let extPolys = null;
        if (selectedPolys.length > 0) {
            extPolys = selectedPolys.map(poly => convToExtPoly(poly));
        } else {
            extPolys = cell.polys.map(poly => convToExtPoly(poly));
        }

        clipboard.writeText(JSON.stringify(extPolys));
    }
    this.undo = function () { }
    this.redo = function () { }
}