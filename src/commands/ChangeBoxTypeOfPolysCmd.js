export function ChangeBoxTypeOfPolysCmd(polys, boxType, changeContentCallback) {
    this.polys = polys;
    this.oldBoxTypes;
    this.boxType = boxType;
    this.changeContentCallback = changeContentCallback;

    this.do = function () {
        this.oldBoxTypes = [];
        this.polys.forEach(poly => {
            this.oldBoxTypes.push(poly.boxType);
            poly.boxType = this.boxType;
        });

        this.changeContentCallback();
    }

    this.undo = function () {
        this.polys.forEach((poly, i) => {
            poly.boxType = this.oldBoxTypes[i];
        });

        this.changeContentCallback();
    }

    this.redo = function () {
        this.polys.forEach(poly => poly.boxType = this.boxType);

        this.changeContentCallback();
    }
}