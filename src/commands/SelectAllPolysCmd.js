import {
    getCell
} from "../boxsheet.js";
import { CmdGroup } from "./CmdGroup";
import { SelectPolyCmd } from "./SelectPolyCmd.js";

export function SelectAllPolysCmd(cells, index, redrawCb) {
    this.cells = cells;
    this.index = index;
    this.redrawCb = redrawCb;
    this.cmdGrp = null;

    this.do = function () {
        const cell = getCell(cells, index);
        const notSelected = cell.polys.filter(p => !p.selected);

        this.cmdGrp = new CmdGroup();
        notSelected.forEach(poly => {
            this.cmdGrp.add(new SelectPolyCmd(poly, true, this.redrawCb));
        });
        this.cmdGrp.do();

        //this.redrawCb();
    }

    this.undo = function () {
        this.cmdGrp.undo();

        //this.redrawCb();
    }


    this.redo = function () {
        this.cmdGrp.redo();

        //this.redrawCb();
    }
}