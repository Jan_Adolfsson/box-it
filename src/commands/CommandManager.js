export function CommandManager() {
    this.historicalCommands = [];
    this.futureCommands = [];
    this.index = -1;

    this.add = function (command) {
        this.historicalCommands.push(command);

        return command;
    }
    this.peek = function () {
        if (this.historicalCommands.length > 0) {
            return this.historicalCommands[this.historicalCommands.length - 1];
        }

        return null;
    }

    this.pop = function () {
        return this.historicalCommands.pop();
    }
    this.do = function (command) {
        command.do();
        this.historicalCommands.push(command);
        this.futureCommands = [];
    }

    this.undo = function () {
        if (this.historicalCommands.length === 0) {
            return;
        }

        const command = this.historicalCommands.pop();
        command.undo();

        this.futureCommands.push(command);
    }

    this.redo = function () {
        if (this.futureCommands.length === 0) {
            return;
        }

        const command = this.futureCommands.pop();
        command.redo();

        this.historicalCommands.push(command);
    }
}
