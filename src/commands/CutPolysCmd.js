import {
    getCell
} from "../boxsheet.js";
import { CmdGroup } from "./CmdGroup";
import { CopyPolysCmd } from "./CopyPolysCmd";
import { RemovePolysCmd } from "./RemovePolysCmd";

export function CutPolysCmd(cells, index, redrawCb) {
    this.cells = cells;
    this.index = index;
    this.redrawCb = redrawCb;
    this.cmdGrp = null;

    this.do = function () {
        const cell = getCell(cells, index);
        const selection = cell.polys.filter(p => p.selected);

        this.cmdGrp = new CmdGroup();
        this.cmdGrp.add(new CopyPolysCmd(cells, index));
        this.cmdGrp.add(new RemovePolysCmd(cell, selection, this.redrawCb));
        this.cmdGrp.do();

        this.redrawCb();
    }

    this.undo = function () {
        this.cmdGrp.undo();

        this.redrawCb();
    }


    this.redo = function () {
        this.cmdGrp.redo();

        this.redrawCb();
    }
}