export function AddPolyCmd(cell, poly, onContentChangeCallback) {
    this.cell = cell;
    this.poly = poly;
    this.onContentChangeCallback = onContentChangeCallback;

    this.do = function () {
        this.cell.polys.push(this.poly);

        this.onContentChangeCallback();
    }

    this.redo = function () {
        this.cell.polys.push(this.poly);
        this.onContentChangeCallback();
    }
    this.undo = function () {
        this.cell.polys.pop();

        this.onContentChangeCallback();
    }
}
