export function RemovePolysCmd(cell, polys, redrawCb) {
    this.cell = cell;
    this.polys = polys;

    this.redrawCb = redrawCb;

    this.do = function () {
        const idxsOfPolysToRmve = this.polys.map(p => this.cell.polys.indexOf(p)).sort().reverse();
        for (let i = 0; i < idxsOfPolysToRmve.length; ++i) {
            this.cell.polys.splice(idxsOfPolysToRmve[i], 1);
        }

        this.redrawCb();
    }

    this.redo = function () {
        this.do();
    }

    this.undo = function () {
        this.polys.forEach(p => this.cell.polys.push(p));

        this.redrawCb();
    }
}