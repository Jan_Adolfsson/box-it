import { filterPolys } from "../boxsheet.js";
import { offsetPolys } from "../shapes/poly.js";

export function MoveCmd(cells, origin, destination, changeContentCallback) {
    this.cells = cells;
    this.origin = origin;
    this.destination = destination;
    this.selection = null;
    this.offset = null;
    this.changeContentCallback = changeContentCallback;

    this.do = function () {
        this.selection = filterPolys(this.cells, p => p.selected);
        this.offset = {
            x: destination.x - this.origin.x,
            y: destination.y - this.origin.y
        };
    }

    this.undo = function () {
        const negativeOffset = {
            x: -this.offset.x,
            y: -this.offset.y
        };
        offsetPolys(this.selection, negativeOffset);

        this.changeContentCallback(this.cells);
    }

    this.redo = function () {
        offsetPolys(this.selection, this.offset);

        this.changeContentCallback(this.cells);
    }
}