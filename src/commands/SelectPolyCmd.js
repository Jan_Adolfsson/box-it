export function SelectPolyCmd(poly, select, redrawCb) {
    this.select = select;
    this.poly = poly;
    this.redrawCb = redrawCb;

    this.do = function () {
        this.poly.selected = this.select;

        this.redrawCb();
    }

    this.redo = function () {
        this.poly.selected = this.select;

        this.redrawCb();
    }

    this.undo = function () {
        this.poly.selected = !this.select;
        this.redrawCb();
    }
}