export function ClearCellSelectionCmd(selection, drawCb) {
    this.selection = selection;
    this.drawCb = drawCb;
    this.cellsRmvd = [];

    this.do = function () {
        this.selection.splice(0)
            .forEach(cell => this.cellsRmvd.push(cell));

        this.drawCb();
    }

    this.undo = function () {
        this.cellsRmvd.forEach(cell => this.selection.push(cell));
        this.drawCb();
    }

    this.redo = function () {
        this.selection.splice(0);
        this.drawCb();
    }
}
