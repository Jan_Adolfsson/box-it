export function ClosePolyCmd(poly, redrawCb) {
    this.poly = poly;
    this.redrawCb = redrawCb;

    this.do = function () {
        this.poly.closed = true;

        this.redrawCb();
    }

    this.redo = function () {
        this.do();
    }

    this.undo = function () {
        this.poly.closed = false;

        this.redrawCb();
    }
}