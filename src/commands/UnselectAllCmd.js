export function UnselectAllCmd(cells, redrawCb) {
    this.cells = cells;
    this.selectedPolys = null;
    this.selectedPts = null;
    this.redrawCb = redrawCb;

    this.do = function () {
        const allPolys = this.cells.flatMap(b => b.polys);
        this.selectedPolys = allPolys.filter(p => p.selected);
        this.selectedPolys.forEach(p => {
            p.selected = false;
        });

        const allPts = allPolys.flatMap(p => p.vertexes);
        this.selectedPts = allPts.filter(p => p.selected);

        this.selectedPts.forEach(p => {
            p.selected = false;
        })

        this.redrawCb();
    }

    this.redo = function () {
        this.selectedPolys.forEach(p => {
            p.selected = false;
        });
        this.selectedPts.forEach(p => {
            p.selected = false;
        })
    }

    this.undo = function () {
        this.selectedPolys.forEach(p => {
            p.selected = true;
        });

        this.selectedPts.forEach(p => {
            p.selected = true;
        })

        this.redrawCb();
    }
}