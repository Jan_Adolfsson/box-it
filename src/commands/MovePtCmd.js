export function MovePtCmd(pt, srcPos, destPos, redrawCb) {
    this.pt = pt;
    this.srcPos = srcPos;
    this.destPos = destPos;
    this.redrawCb = redrawCb;

    this.do = function () {
        this.pt.x = destPos.x;
        this.pt.y = destPos.y;

        this.redrawCb();
    }

    this.redo = function () {
        this.pt.x = destPos.x;
        this.pt.y = destPos.y;

        this.redrawCb();
    }

    this.undo = function () {
        this.pt.x = srcPos.x;
        this.pt.y = srcPos.y;

        this.redrawCb();
    }
}