export function CmdGroup() {
    this.commands = [];

    this.add = function (command) {
        this.commands.push(command);
    }
    this.do = function () {
        this.commands.forEach(c => c.do());
    }

    this.redo = function () {
        this.commands.forEach(c => c.redo());
    }

    this.undo = function () {
        this.commands.slice()
            .reverse()
            .forEach(c => c.undo());
    }
    this.pop = function () {
        return this.commands.pop();
    }
}