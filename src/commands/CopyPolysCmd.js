const { clipboard } = require('electron');

import { getCell } from "../boxsheet.js";

export const CLIPBOARD_FORMATS = {
    POLYS: "BoxIt.Polygons",
    CELLS: "BoxIt.Cells"
};
export function CopyPolysCmd(cells, index) {
    this.cells = cells;
    this.index = index;

    this.do = function () {
        const cell = getCell(this.cells, this.index);
        const selection = cell.polys.filter(p => p.selected);
        const data = {
            format: CLIPBOARD_FORMATS.POLYS,
            selection
        };
        const json = JSON.stringify(data);
        clipboard.writeText(json);
    }
    this.undo = function () { }
    this.redo = function () { }
}