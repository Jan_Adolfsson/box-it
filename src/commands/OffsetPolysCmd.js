import { offsetPts } from "../shapes/poly.js";

export function OffsetPolysCmd(polysPts, offset, changeContentCallback) {
    this.polysPts = polysPts;
    this.offset = offset;
    this.changeContentCallback = changeContentCallback;

    this.do = function () {
    }

    this.undo = function () {
        const negativeOffset = {
            x: -this.offset.x,
            y: -this.offset.y
        };
        offsetPts(this.polysPts.flatMap(pt => pt), negativeOffset);

        this.changeContentCallback();
    }

    this.redo = function () {
        offsetPts(this.polysPts.flatMap(pt => pt), this.offset);

        this.changeContentCallback();
    }
}