import { offsetPts } from "../shapes/poly.js";

export function OffsetPtsCmd(pts, offset, changeContentCallback) {
    this.pts = pts;
    this.offset = offset;
    this.changeContentCallback = changeContentCallback;

    this.do = function () {
    }

    this.undo = function () {
        const negativeOffset = {
            x: -this.offset.x,
            y: -this.offset.y
        };
        offsetPts(this.pts, negativeOffset);

        this.changeContentCallback();
    }

    this.redo = function () {
        offsetPts(this.pts, this.offset);

        this.changeContentCallback();
    }
}