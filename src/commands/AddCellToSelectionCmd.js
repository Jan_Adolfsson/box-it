export function AddCellToSelectionCmd(selection, cell, drawCb) {
    this.selection = selection;
    this.cell = cell;
    this.cellAdded = null;
    this.drawCb = drawCb;


    this.do = function () {
        this.cellAdded = { x: this.cell.x, y: this.cell.y };
        this.selection.push(this.cellAdded);

        this.drawCb();
    }

    this.undo = function () {
        this.selection.pop();
        this.drawCb();
    }

    this.redo = function () {
        this.selection.push(this.cellAdded);
        this.drawCb();
    }
}
