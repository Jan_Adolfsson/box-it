export function SelectPtCmd(pt, select, redrawCb) {
    this.pt = pt;
    this.select = select;

    this.redrawCb = redrawCb;

    this.do = function () {
        this.pt.selected = this.select;

        this.redrawCb();
    }

    this.undo = function () {
        this.pt.selected = !this.select;

        this.redrawCb();
    }

    this.redo = function () {
        this.pt.selected = this.select;

        this.redrawCb();
    }
}
