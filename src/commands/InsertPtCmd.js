export function InsertPtCmd(poly, pt, lineSegment, redrawCb) {
    this.poly = poly;
    this.pt = pt;
    this.lineSegment = lineSegment;
    this.redrawCb = redrawCb;
    this.index = null;

    this.do = function () {
        this.index = this.poly.vertexes.indexOf(this.lineSegment.end);
        this.poly.vertexes.splice(this.index, 0, this.pt);

        this.redrawCb();
    }

    this.redo = function () {
        this.poly.vertexes.splice(this.index, 1, this.pt);

        this.redrawCb();
    }

    this.undo = function () {
        this.poly.vertexes.splice(this.index, 1);

        this.redrawCb();
    }
}