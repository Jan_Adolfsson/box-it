export function AddPtCmd(poly, pt, redrawCb) {
    this.poly = poly;
    this.pt = pt;
    this.redrawCb = redrawCb;

    this.do = function () {
        this.poly.vertexes.push(pt);

        this.redrawCb();
    }

    this.redo = function () {
        this.poly.vertexes.push(pt);

        this.redrawCb();
    }

    this.undo = function () {
        this.poly.vertexes.pop();

        this.redrawCb();
    }
}