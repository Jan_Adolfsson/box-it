export function RemoveCellFromSelectionCmd(selection, idx, drawCb) {
    this.selection = selection;
    this.idx = idx;
    this.drawCb = drawCb;
    this.removedCell = null;


    this.do = function () {
        this.removedCell = this.selection.splice(this.idx, 1)[0];

        this.drawCb();
    }

    this.undo = function () {
        this.selection.splice(this.idx, 0, this.removedCell);

        this.drawCb();
    }

    this.redo = function () {
        this.selection.splice(this.idx, 1);

        this.drawCb();
    }
}
