export function RemovePtsCmd(poly, pointIndexPairs, redrawCb) {
    this.poly = poly;
    this.pointIndexPairs = pointIndexPairs;
    this.redrawCb = redrawCb;

    this.do = function () {
        const sortedDesc = this.pointIndexPairs.sort((a, b) => b.index - a.index);
        for (let i = 0; i < sortedDesc.length; ++i) {
            this.poly.vertexes.splice(sortedDesc[i].index, 1);
        }

        this.redrawCb();
    }

    this.redo = function () {
        this.do();
    }

    this.undo = function () {
        const sortedAsc = this.pointIndexPairs.sort((a, b) => a.index - b.index);
        sortedAsc.forEach(pip => this.poly.vertexes.splice(pip.index, 0, pip.pt));

        this.redrawCb();
    }
}