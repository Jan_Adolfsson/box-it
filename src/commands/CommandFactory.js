import { AddPtCmd } from "./AddPtCmd.js";
import { AddPolyCmd } from "./AddPolyCmd.js";
import { ChangeBoxTypeOfPolysCmd } from "./ChangeBoxTypeOfPolysCmd.js";
import { ClosePolyCmd } from "./ClosePolyCmd.js";
import { CopyPolysCmd } from "./CopyPolysCmd";
import { CutPolysCmd } from "./CutPolysCmd";
import { ExportPolysCmd } from "./ExportPolysCmd";
import { InsertPtCmd } from "./InsertPtCmd.js";
import { RemovePtsCmd } from "./RemovePtsCmd.js";
import { RemovePolysCmd } from "./RemovePolysCmd.js";
import { MovePtCmd } from "./MovePtCmd.js";
import { OffsetPtsCmd } from "./OffsetPtsCmd.js";
import { OffsetPolysCmd } from "./OffsetPolysCmd.js";
import { PastePolysCmd } from "./PastePolysCmd";
import { ResizeRectCmd } from "./ResizeRectCmd.js";
import { SelectPtCmd } from "./SelectPtCmd.js";
import { SelectPolyCmd } from "./SelectPolyCmd.js";
import { UnselectAllCmd } from "./UnselectAllCmd.js";

import { getCell } from "../boxsheet.js";
import { SelectAllPolysCmd } from "./SelectAllPolysCmd.js";

export function CommandFactory() {
    this.cells = null;
    this.index = null;
    this.redrawCb = null;
    this.isLogOn = false;
    const log = logger.bind(this);

    this.setCells = function (cells) {
        this.cells = cells;
    }
    this.setIndex = function (index) {
        this.index = index;
    }
    this.setRedrawCallback = function (redrawCb) {
        this.redrawCb = redrawCb;
    }

    this.addPoly = function (cell, poly) {
        return log(new AddPolyCmd(cell, poly, this.redrawCb));
    }
    this.addPt = function (poly, pt) {
        return log(new AddPtCmd(poly, pt, this.redrawCb));
    }
    this.cutPolys = function () {
        return log(new CutPolysCmd(this.cells, this.index, this.redrawCb));
    }
    this.changeBoxTypeOfPolys = function (boxType) {
        const cell = getCell(this.cells, this.index);
        const selection = cell.polys.filter(poly => poly.selected);
        return log(new ChangeBoxTypeOfPolysCmd(selection, boxType, this.redrawCb));
    }
    this.deletePolys = function () {
        const cell = getCell(this.cells, this.index);
        const selection = cell.polys.filter(poly => poly.selected);
        return log(new RemovePolysCmd(cell, selection, this.redrawCb));
    }
    this.copyPolys = function () {
        return log(new CopyPolysCmd(this.cells, this.index));
    }

    this.pastePolys = function () {
        return log(new PastePolysCmd(this.cells, this.index, this.redrawCb));
    }
    this.closePoly = function (poly) {
        return log(new ClosePolyCmd(poly, this.redrawCb));
    }
    this.exportPolysToClipboard = function () {
        return log(new ExportPolysCmd(this.cells, this.index));
    }
    this.insertPt = function (poly, pt, lineSegment) {
        return log(new InsertPtCmd(poly, pt, lineSegment, this.redrawCb));
    }
    this.removePts = function (poly, pointIndexPairs) {
        return log(new RemovePtsCmd(poly, pointIndexPairs, this.redrawCb));
    }
    this.removePolys = function (cell, polys) {
        return log(new RemovePolysCmd(cell, polys, this.redrawCb));
    }
    this.movePt = function (pt, srcPos, destPos) {
        return log(new MovePtCmd(pt, srcPos, destPos, this.redrawCb));
    }
    this.offsetPts = function (pts, offset) {
        return log(new OffsetPtsCmd(pts, offset, this.redrawCb));
    }
    this.resizePolys = function (polysPts, offset) {
        return log(new OffsetPolysCmd(polysPts, offset, this.redrawCb));
    }
    this.selectAllPolys = function () {
        return log(new SelectAllPolysCmd(this.cells, this.index, this.redrawCb));
    }
    this.selectPoly = function (poly) {
        return log(new SelectPolyCmd(poly, true, this.redrawCb));
    }
    this.selectPt = function (pt) {
        return log(new SelectPtCmd(pt, true, this.redrawCb));
    }
    this.unselectPt = function (pt) {
        return log(new SelectPtCmd(pt, false, this.redrawCb));
    }
    this.unselectPoly = function (pt) {
        return log(new SelectPolyCmd(pt, false, this.redrawCb));
    }
    this.unselectAll = function (cells) {
        return log(new UnselectAllCmd(cells, this.redrawCb));
    }
    this.resizeRect = function (rect, oldPts) {
        return log(new ResizeRectCmd(rect, oldPts, this.redrawCb));
    }

    function logger(obj) {
        if (this.isLogOn) {
            console.log(obj);
        }
        return obj;
    }
}