export function SetToolStateCmd(tool, state, redrawCb) {
    this.tool = tool;
    this.newState = state;
    this.redrawCb = redrawCb;
    this.oldState = null;

    this.do = function () {
        this.oldState = this.tool.state;
        this.tool.state = this.newState;

        this.redrawCb();
    }

    this.redo = function () {
        this.do();
    }

    this.undo = function () {
        this.tool.state = this.oldState;
        this.oldState = null;

        this.redrawCb();
    }
}