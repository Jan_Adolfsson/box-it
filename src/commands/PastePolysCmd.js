const { clipboard } = require('electron');

import { createNewCell, getCell } from "../boxsheet.js";
import { CLIPBOARD_FORMATS } from "./CopyPolysCmd.js";

export function PastePolysCmd(cells, index, redrawCb) {
    this.cells = cells;
    this.index = index;
    this.redrawCb = redrawCb;
    this.cell = null;
    this.polys = null;
    self = this;
    this.do = function () {
        const clipText = clipboard.readText();
        if (!clipText || clipText === "") {
            return;
        }

        const data = JSON.parse(clipText);
        if (!data || !data.format || data.format !== CLIPBOARD_FORMATS.POLYS || data.selection.length === 0) {
            return;
        }

        this.cell = getCell(cells, index);
        if (!this.cell) {
            this.cell = createNewCell(index);
        }
        this.polys = data.selection;
        paste(this.cell, this.polys);
        self.redrawCb();
    }

    this.undo = function () {
        const cell = getCell(this.cells, this.index);
        cell.polys.splice(cell.polys.length - this.polys.length);

        this.redrawCb();
    }

    this.redo = function () {
        if (!this.polys) {
            return;
        }

        paste(this.cell, this.polys)

        this.redrawCb();
    }
    function paste(cell, polys) {
        const copyOfPolys = JSON.parse(JSON.stringify(polys));
        cell.polys.push(...copyOfPolys);
    }
}