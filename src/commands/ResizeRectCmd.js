export function ResizeRectCmd(rect, oldPts, redrawCb) {
    this.rect = rect;
    this.oldPts = oldPts;
    this.newPts = null;

    this.redrawCb = redrawCb;

    this.do = function () {
    }

    this.undo = function () {
        this.newPts = JSON.parse(JSON.stringify(this.rect.vertexes));

        for (let i = 0; i < 4; ++i) {
            this.rect.vertexes[i].x = this.oldPts[i].x;
            this.rect.vertexes[i].y = this.oldPts[i].y;
        }

        this.redrawCb();
    }

    this.redo = function () {
        for (let i = 0; i < 4; ++i) {
            this.rect.vertexes[i].x = this.newPts[i].x;
            this.rect.vertexes[i].y = this.newPts[i].y;
        }

        this.redrawCb();
    }
}