export function ExtendCellSelectionCmd(selection, cells, drawCb) {
    this.selection = selection;
    this.cells = cells;
    this.drawCb = drawCb;
    this.cellsToAdd = null;


    this.do = function () {
        this.cellsToAdd = this.cells.filter(
            cell => !this.selection.find(i => i.x === cell.x && i.y === cell.y)
        );

        this.selection.push(...this.cellsToAdd);

        this.drawCb();
    }

    this.undo = function () {
        this.selection.splice(this.selection.length - this.cellsToAdd.length);
      

        this.drawCb();
    }

    this.redo = function () {
        this.selection.push(...this.cellsToAdd);

        this.drawCb();
    }
}
