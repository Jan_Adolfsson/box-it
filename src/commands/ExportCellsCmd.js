const { clipboard } = require('electron')

import { getCells, convToExtCells } from "../boxsheet.js";

export function ExportCellsCmd(cells, selection) {
    this.cells = cells;
    this.selection = selection;

    this.do = function () {
        const selectedCells = getCells(this.cells, this.selection);
        const extCells = convToExtCells(selectedCells);
        const json = JSON.stringify(extCells);
        clipboard.writeText(json);
    }
    this.undo = function () { }
    this.redo = function () { }
}