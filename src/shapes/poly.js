export const POLY_H_AREA = {
    LEFT: 0,
    RIGHT: 1
};
export const POLY_V_AREA = {
    TOP: 0,
    BOTTOM: 1
};

export function angle(pt1, pt2) {
    var dy = pt2.y - pt1.y;
    var dx = pt2.x - pt1.x;
    var angle = Math.atan2(dy, dx);

    return angle;
}

export function areaOfPoly(poly, pt) {
    const area = {
        x: null,
        y: null
    };
    const ctr = centerOfPoly(poly.vertexes);
    area.x = pt.x < ctr.x ? POLY_H_AREA.LEFT : POLY_H_AREA.RIGHT;
    area.y = pt.y < ctr.y ? POLY_V_AREA.TOP : POLY_V_AREA.BOTTOM
    return area;
}

export function boundingBoxOfPolyScaled(vtxs, scaleFac) {
    const vtxsCopied = vtxs.map(vtx => {
        return {
            x: vtx.x,
            y: vtx.y
        };
    });

    scalePoly(vtxsCopied, scaleFac);

    return boundingBoxOfPoly(vtxsCopied);
}

export function centerOfPoly(vtxs) {
    let xMax = Number.NEGATIVE_INFINITY;
    let yMax = Number.NEGATIVE_INFINITY;
    let xMin = Number.POSITIVE_INFINITY;
    let yMin = Number.POSITIVE_INFINITY;

    vtxs.forEach(vtx => {
        if (vtx.x > xMax) {
            xMax = vtx.x;
        }
        if (vtx.x < xMin) {
            xMin = vtx.x;
        }
        if (vtx.y > yMax) {
            yMax = vtx.y;
        }
        if (vtx.y < yMin) {
            yMin = vtx.y;
        }
    });

    return {
        x: (xMax + xMin) / 2,
        y: (yMax + yMin) / 2,
    };
}
export function boundingBoxOfPoly(vtxs) {
    let left = Infinity,
        top = Infinity,
        right = 0,
        bottom = 0;

    vtxs.forEach(pt => {
        left = Math.min(left, pt.x);
        top = Math.min(top, pt.y);
        right = Math.max(right, pt.x);
        bottom = Math.max(bottom, pt.y);
    })

    return {
        x: left,
        y: top,
        width: right - left,
        height: bottom - top
    };
}

export function convToExtPoly(poly) {
    return {
        vertexes: convToExtVtxs(poly.vertexes),
        boxType: poly.boxType ? poly.boxType : "Hurt",
        tags: []
    };
}

export function convToExtPolys(polys) {
    return polys.map(poly => convToExtPoly(poly));
}

export function convToExtVtx(vtx) {
    return {
        x: vtx.x,
        y: vtx.y
    }
}

export function convToExtVtxs(vtxs) {
    return vtxs.map(vtx => convToExtVtx(vtx));
}

export function createPoly(boxType, closed, selected) {
    return {
        vertexes: [],
        boxType: boxType,
        tags: [],
        closed: closed,
        selected: selected
    };
}

export function createPt(x, y, zoom) {
    return {
        x: x / (zoom && zoom > 0 ? zoom : 1),
        y: y / (zoom && zoom > 0 ? zoom : 1)
    };
}

export function createRect(boxType, x, y, zoom, selected) {
    const poly = createPoly(boxType, true, selected);

    for (let i = 0; i < 4; ++i) {
        poly.vertexes.push(createPt(x, y, zoom));
    }

    return poly;
}

export function distance(pt1, pt2) {
    const a = pt1.x - pt2.x;
    const b = pt1.y - pt2.y;
    const c = Math.sqrt(a * a + b * b);
    return c;
}

export function filterPts(polys, filter) {
    return polys
        .flatMap(poly => poly.vertexes)
        .filter(v => filter(v));
}

export function hitWithVtxClosestToPt(hits, pt) {
    let closest = null;

    for (let i = 0; i < hits.length; ++i) {
        if (closest === null) {
            closest = hits[i];
        }

        if (distance(pt, hits[i].pt) < distance(pt, closest.pt)) {
            closest = hits[i];
        }
    }

    return closest;
}

export function isRect(poly) {
    if (poly.vertexes.length !== 4) {
        return false;
    }

    const pt0 = poly.vertexes[0];
    const pt1 = poly.vertexes[1];
    const pt2 = poly.vertexes[2];
    const pt3 = poly.vertexes[3];

    return pt0.x === pt3.x &&
        pt0.y === pt1.y &&
        pt2.x === pt1.x &&
        pt2.y == pt3.y;
}

export function lineSegsCloseToPt(poly, pt, maxDist) {
    const segHits = [];

    for (let i = 0; i < poly.vertexes.length; ++i) {
        const seg = createLnSeg(poly.vertexes, i);
        const dist = ptToLineSegDist(pt, seg);
        if (dist <= maxDist) {
            segHits.push(createSegHit(poly, pt, seg, dist))
        }
    }

    return segHits;

    function createLnSeg(vertexes, startIdx) {
        const endIdx = startIdx === vertexes.length - 1 ? 0 : startIdx + 1;
        return {
            start: vertexes[startIdx],
            end: vertexes[endIdx]
        };
    }
    function createSegHit(poly, pt, seg, dist) {
        return {
            poly: poly,
            pt: pt,
            segment: seg,
            distance: dist
        };
    }
}

export function offsetPt(pt, offset) {
    pt.x += offset.x;
    pt.y += offset.y;
}

export function offsetPts(pts, offset) {
    pts.forEach(p => offsetPt(p, offset));
}

export function offsetPoly(poly, offset) {
    poly.vertexes.forEach(v => offsetPt(v, offset));
}

export function offsetPolys(polys, offset) {
    polys.forEach(p => {
        offsetPoly(p, offset);
    });
}

export function polyClosestToPt(polys, pt, maxDist) {
    const hits = polysCloseToPt(polys, pt, maxDist);
    if (hits.length === 0) {
        return null;
    }

    return hits.reduce((acc, curr) => curr.distance < acc.distance ? curr : acc);
}

export function polysCloseToPt(polys, pt, maxDist) {
    let allHits = [];

    polys.forEach(poly => {
        const hits = lineSegsCloseToPt(poly, pt, maxDist);
        if (hits.length > 0) {
            allHits = allHits.concat(hits);
        }
    });

    return allHits;
}

// Solution found at:
// http://alienryderflex.com/poly/
export function polyContainsPt(poly, x, y) {
    const vertexes = poly.vertexes;
    let j = vertexes.length - 1;
    let oddNodes = false;

    for (let i = 0; i < vertexes.length; i++) {
        if (vertexes[i].y < y && vertexes[j].y >= y
            || vertexes[j].y < y && vertexes[i].y >= y) {
            if (vertexes[i].x + (y - vertexes[i].y) / (vertexes[j].y - vertexes[i].y) * (vertexes[j].x - vertexes[i].x) < x) {
                oddNodes = !oddNodes;
            }
        }
        j = i;
    }
    return oddNodes;
}

export function polysContainingPt(polys, pt) {
    const hits = [];

    polys.forEach(poly => {
        if (polyContainsPt(poly, pt.x, pt.y)) {
            hits.push(poly);
        }
    });

    return hits;
}

export function polyWithVtxCloseToPt(polys, pt, maxDist) {
    for (let i = 0; i < polys.length; ++i) {

        const poly = polys[i];
        for (let j = 0; j < poly.vertexes.length; ++j) {

            const vtx = poly.vertexes[j];
            if (distance(pt, vtx) <= maxDist) {
                return {
                    poly: poly,
                    pt: vtx
                };
            }
        }
    }
    return null;
}

export function polysWithVtxsCloseToPt(polys, pt, maxDist) {
    const hits = [];

    for (let i = 0; i < polys.length; ++i) {
        const poly = polys[i];
        const hit = vtxClosestToPt(poly, pt, maxDist);
        if (hit) {
            hits.push(hit);
        }
    }
    return hits;
}

export function ptToLineSegDist(pt, lineSeg) {
    const
        A = distance(pt, lineSeg.start),
        B = distance(lineSeg.end, pt),
        C = distance(lineSeg.end, lineSeg.start);

    if (Math.pow(B, 2) > Math.pow(A, 2) + Math.pow(C, 2)) {
        return A;
    } else if (Math.pow(A, 2) > Math.pow(B, 2) + Math.pow(C, 2)) {
        return B;
    } else {
        const s = (A + B + C) / 2
        return 2 / C * Math.sqrt(s * (s - A) * (s - B) * (s - C))
    }
}

export function rectContainsPoly(rect, poly) {
    const cell = boundingBoxOfPoly(poly.vertexes);

    return rect.x <= cell.x &&
        rect.y <= cell.y &&
        rect.x + rect.width >= cell.x + cell.width &&
        rect.y + rect.height >= cell.y + cell.height;
}

export function scalePoly(vtxs, scaleFac) {
    const ctr = centerOfPoly(vtxs);
    vtxs.forEach(vtx => {
        vtx.x = (scaleFac.x * (vtx.x - ctr.x) + ctr.x);
        vtx.y = (scaleFac.y * (vtx.y - ctr.y) + ctr.y);
    })
}
export function vtxClosestToPt(poly, pt, maxDist) {
    for (let j = 0; j < poly.vertexes.length; ++j) {
        const vtx = poly.vertexes[j];
        if (distance(pt, vtx) <= maxDist) {
            return {
                poly: poly,
                pt: vtx
            };
        }
    }

    return null;
}