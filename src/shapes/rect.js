const CORNERS = {
    TOP_LEFT: 0,
    TOP_RIGHT: 1,
    BOTTOM_RIGHT: 2,
    BOTTOM_LEFT: 3
};

export function resizeByCorner(pts, corner, x, y, zoom) {
    if (corner < 0 || corner > 3) {
        console.log("Invalid corner index.")
        throw "Invalid corner index.";
    }

    if (corner === CORNERS.TOP_LEFT) {
        resizeByTopLeft(pts, x, y, zoom);
    } else if (corner === CORNERS.TOP_RIGHT) {
        resizeByTopRight(pts, x, y, zoom);
    }
    else if (corner === CORNERS.BOTTOM_RIGHT) {
        resizeByBottomRight(pts, x, y, zoom);
    } else if (corner === CORNERS.BOTTOM_LEFT) {
        resizeByBottomLeft(pts, x, y, zoom);
    }

    function resizeByTopLeft(pts, x, y, zoom) {
        pts[0].x = x / zoom;
        pts[0].y = y / zoom;

        pts[1].y = y / zoom;

        pts[3].x = x / zoom;
    };
    function resizeByTopRight(pts, x, y, zoom) {
        pts[0].y = y / zoom;

        pts[1].x = x / zoom;
        pts[1].y = y / zoom;

        pts[2].x = x / zoom;
    };
    function resizeByBottomRight(pts, x, y, zoom) {
        pts[1].x = x / zoom;

        pts[2].x = x / zoom;
        pts[2].y = y / zoom;

        pts[3].y = y / zoom;
    };
    function resizeByBottomLeft(pts, x, y, zoom) {
        pts[0].x = x / zoom;

        pts[2].y = y / zoom;

        pts[3].x = x / zoom;
        pts[3].y = y / zoom;
    };
};

export function rectClosestToPt(rects, pt) {
    let rect = null;
    let shortest = null;

    rects.forEach(r => {
        const dist = shortestPtToSideDist(r.vertexes, pt);
        if (!shortest) {
            shortest = dist;
            rect = r;
        }
        if (dist < shortest) {
            shortest = dist;
            rect = r;
        }
    });

    return rect;

    function shortestPtToSideDist(pts, pt) {
        const distances = [
            pt.y - pts[0].y,
            pts[1].x - pt.x,
            pts[2].y - pt.y,
            pt.x - pts[0].x
        ];

        let shortest = null;
        distances.forEach(d => {
            if (!shortest) {
                shortest = d;
            }
            if (d < shortest) {
                shortest = d;
            }
        });

        return shortest;
    };
}

