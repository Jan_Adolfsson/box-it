const fs = require("fs");

export function loadPainterSettings(path) {
    console.log(`Load painter settings from path: ${path}`);

    const json = fs.readFileSync(path, { encoding: "utf-8" });
    const settings = JSON.parse(json);

    console.log(`Painter settings loaded as ${json}`);

    return settings;
}

export function createDefaultPainterSettings() {
    return {
        canvas: {
            showGrid: true,
            gridColor: "#dedede",
            bgColor: "#ffffff"
        },
        selection: {
            lineColor: "#e8a527",
            fillColor: "#e8a527"
        },
        polygon: {
            fill: false,
            fillColor: "#00a2ff",
            defaultColor: "#00a2ff"
        }
    };
}

export const savePainterSettings = (path, settings) => {
    const json = JSON.stringify(settings);

    console.log(`Save Painter settings ${json} to path: ${path}`);

    fs.writeFileSync(path, json, { encoding: "utf-8" });
}
