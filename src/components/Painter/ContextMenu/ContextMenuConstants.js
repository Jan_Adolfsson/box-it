export const ContextMenuItemType = {
    Button: "Button",
    CheckButton: "CheckButton",
    Group: "Group",
    RadioButton: "RadioButton",
    Ruler: "Ruler",
}