import { Tool, TOOLS } from "./tool.js";
import { IdleSelectionOp } from "./selection/idleSelectionOp";
import { IdleRectOp } from "./rect/idleRectOp.js";
import { IdlePolyOp } from "./poly/idlePolyOp.js";
import { IdleResizeOp } from "./resize/idleResizeOp.js";
import { IdlePtOp } from "./point/idlePtOp.js";

export function ToolFactory(cmdMgr, cmdFty) {
    this.cmdMgr = cmdMgr;
    this.cmdFty = cmdFty;
    this.painter = null;
    this.isLogOn = false;
    let log = logger.bind(this);

    this.setPainterInfo = function (painterInfo) {
        this.painter = painterInfo;
    }
    this.getPainterInfo = function () {
        return this.painter;
    }
    this.create = function (name) {
        switch (name) {
            case TOOLS.TWEAK:
                return this.point();

            case TOOLS.RECT:
                return this.rect();

            case TOOLS.POLY:
                return this.poly();

            case TOOLS.RESIZE:
                return this.resize();

            case TOOLS.SELECT:
                return this.select();

            default:
                console.log(`Unknown tool name ${name}`);
                break;
        }
    }
    this.select = function () {
        const idle = new IdleSelectionOp(this.cmdMgr, this.cmdFty, this.painter, null);
        return log(new Tool(TOOLS.SELECT, this.cmdMgr, this.cmdFty, idle));
    }
    this.rect = function () {
        const idle = new IdleRectOp(this.cmdMgr, this.cmdFty, this.painter, null);
        return log(new Tool(TOOLS.RECT, this.cmdMgr, this.cmdFty, idle));
    }
    this.resize = function () {
        const idle = new IdleResizeOp(this.cmdMgr, this.cmdFty, this.painter, null);
        return log(new Tool(TOOLS.RESIZE, this.cmdMgr, this.cmdFty, idle));
    }
    this.poly = function () {
        const idle = new IdlePolyOp(this.cmdMgr, this.cmdFty, this.painter, null);
        return log(new Tool(TOOLS.POLY, this.cmdMgr, this.cmdFty, idle));
    }
    this.point = function () {
        const idle = new IdlePtOp(this.cmdMgr, this.cmdFty, this.painter, null);
        return log(new Tool(TOOLS.POINTS, this.cmdMgr, this.cmdFty, idle));
    }
    function logger(obj) {
        if (this.isLogOn) {
            console.log(obj);
        }
        return obj;
    }
}
