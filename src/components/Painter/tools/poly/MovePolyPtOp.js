import { Operation } from "../operation.js";
import { IdlePolyOp } from "./idlePolyOp.js";
import { CmdGroup } from "../../../../commands/CmdGroup.js";

export class MovePolyPtOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb, hit) {
        super(cmdMgr, cmdFty, painter, redrawCb);
        this.canvas = null;
        this.hit = hit;
        this.scrPos = null;
        this.cmdGrp = null;
    }
    onMouseDown(e) {
        this.srcPos = { x: this.hit.pt.x, y: this.hit.pt.y };

        this.cmdGrp = new CmdGroup();
        this.cmdMgr.add(this.cmdGrp);

        this.cmdGrp.add(this.cmdFty.unselectAll(this.painter.cells));
        this.cmdGrp.add(this.cmdFty.selectPt(this.hit.pt));
        this.cmdGrp.do();
    }
    onMouseUp(e) {
        const destPos = { x: this.hit.pt.x, y: this.hit.pt.y };
        this.cmdGrp.add(this.cmdFty.movePt(this.hit.pt, this.srcPos, destPos));

        this.painter.tool.setOperation(new IdlePolyOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb));
    }
    onMouseMove(e) {
        this.hit.pt.x = e.offsetX / this.painter.zoom;
        this.hit.pt.y = e.offsetY / this.painter.zoom;

        this.redrawCb();
    }
}
