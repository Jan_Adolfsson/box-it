import { getCell } from "../../../../boxsheet.js";
import {
    hitWithVtxClosestToPt,
    createPt
}
    from "../../../../shapes/poly.js";
import { Operation } from "../operation.js";
import { DrawPolyOp } from "./drawPolyOp";
import { MovePolyPtOp } from "./MovePolyPtOp.js";

export class IdlePolyOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb) {
        super(cmdMgr, cmdFty, painter, redrawCb);
    }

    onMouseDown(e, modKeys) {
        const cell = getCell(this.painter.cells, this.painter.index);
        this.drawPolyOp(e, modKeys, cell);
    }
    onMouseUp(e, modKeys) {
    }
    onMouseMove(e, modKeys) {
    }

    drawPolyOp(e, modKeys, cell) {
        const op = new DrawPolyOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb, cell);
        this.painter.tool.setOperation(op);
        op.onMouseDown(e, modKeys);
    }

    movePolyPtOp(e, modKeys, closestHit) {
        const op = new MovePolyPtOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb, closestHit);
        this.painter.tool.setOperation(op);
        op.onMouseDown(e, modKeys);
    }
}