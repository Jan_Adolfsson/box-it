import { CmdGroup } from "../../../../commands/CmdGroup.js";
import {
    createPoly,
    createPt,
    distance
} from "../../../../shapes/poly.js";

import { Operation } from "../operation.js";
import { IdlePolyOp } from "./idlePolyOp.js";

const POINT_HIT_TEST_DISTANCE = 5;

export class DrawPolyOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb, cell) {
        super(cmdMgr, cmdFty, painter, redrawCb);
        this.cell = cell;
        this.poly = null;
        this.startPt = null;
        this.cmdBatch = null;
    }

    abort() {
        this.cmdMgr.pop().undo();
        this.painter.tool.setOperation(new IdlePolyOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb));
    }
    finalize() {
        if (this.poly) {
            this.closePoly();
            this.painter.tool.setOperation(new IdlePolyOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb));
        }
    }
    onMouseDown(e) {
        const pt = createPt(e.offsetX, e.offsetY, this.painter.zoom);
        if (this.poly) {
            if (distance(pt, this.startPt) <= POINT_HIT_TEST_DISTANCE) {
                this.closePoly();
                this.painter.tool.setOperation(new IdlePolyOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb));
                this.redrawCb();
            } else {
                this.appendPt(pt);
            }
        } else {
            this.addStartPt(pt);
            const secondPt = createPt(e.offsetX, e.offsetY, this.painter.zoom);
            this.appendPt(secondPt);
        }
    }
    addStartPt(pt) {
        this.cmdBatch = new CmdGroup();
        this.cmdMgr.add(this.cmdBatch);

        this.startPt = pt;

        this.poly = createPoly(this.painter.boxType, false, true);
        this.poly.vertexes.push(this.startPt);

        const cmdGrp = new CmdGroup();
        cmdGrp.add(this.cmdFty.unselectAll(this.painter.cells))
        cmdGrp.add(this.cmdFty.addPoly(this.cell, this.poly));
        cmdGrp.add(this.cmdFty.selectPt(this.startPt));
        cmdGrp.do();

        this.cmdBatch.add(cmdGrp);
    }


    closePoly() {
        this.cmdBatch.pop().undo();

        const cmdGrp = new CmdGroup();
        cmdGrp.add(this.cmdFty.closePoly(this.poly));
        cmdGrp.add(this.cmdFty.unselectPt(this.getLastPt()));
        cmdGrp.add(this.cmdFty.selectPt(this.startPt));
        cmdGrp.add(this.cmdFty.selectPoly(this.poly));
        cmdGrp.do();

        this.cmdBatch.add(cmdGrp);
    }
    appendPt(pt) {
        const cmdGrp = new CmdGroup();

        cmdGrp.add(this.cmdFty.unselectPt(this.getLastPt()));
        cmdGrp.add(this.cmdFty.addPt(this.poly, pt));
        cmdGrp.add(this.cmdFty.selectPt(pt));
        cmdGrp.do();

        this.cmdBatch.add(cmdGrp);
    }
    onMouseMove(e) {
        const pt = this.getLastPt();
        pt.x = e.offsetX / this.painter.zoom;
        pt.y = e.offsetY / this.painter.zoom;

        if (this.redrawCb) {
            this.redrawCb();
        }
    }
    getLastPt() {
        return this.poly.vertexes[this.poly.vertexes.length - 1];
    }
}
