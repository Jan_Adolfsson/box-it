export const TOOLS = {
    TWEAK: "tweak",
    POLY: "poly",
    RECT: "rect",
    RESIZE: "resize",
    SELECT: "select"
};
const MODIFIER_KEYS = {
    SHIFT: "Shift",
    CTRL: "Control",
    ALT: "Alt"
};
export class Tool {
    constructor(name, cmdMgr, cmdFty, idleOperation) {
        this.name = name;
        this.cmdMgr = cmdMgr;
        this.cmdFty = cmdFty;
        this.canvas = null;
        this.poly = null;
        this.keys = {};
        this.modKeys = {
            shift: false,
            ctrl: false,
            alt: false,

            anyPressed: function () {
                return shift || ctrl || alt;
            }
        };
        this.operation = idleOperation;
        if (this.operation) {
            this.operation.setTool(this);
        }
    }

    setIndex(index) {
        this.operation.setIndex(index);
    }
    setOperation(operation) {
        this.operation = operation;
    }
    setBoxType(boxType) {
        const painterInfo = this.operation.getPainterInfo();
        painterInfo.boxType = boxType;
    }
    setCells(cells) {
        this.operation.setCells(cells);
    }
    setZoom(zoom) {
        this.operation.setZoom(zoom);
    }
    active() { };
    abort() {
        this.operation.abort();
    }
    onContentChange(callback) {
        this.operation.onContentChange(callback);
    }
    attachCanvas(canvas) {
        this.canvas = canvas;
        this.canvas.addEventListener("mousedown", this);
        this.canvas.addEventListener("mousedown", this);
        this.canvas.addEventListener("mouseup", this);
        this.canvas.addEventListener("dblclick", this);
        this.canvas.addEventListener("mousemove", this);
        window.addEventListener("keydown", this);
        window.addEventListener("keyup", this);
    }
    detachCanvas() {
        if (this.canvas) {
            this.canvas.removeEventListener("mousedown", this);
            this.canvas.removeEventListener("mouseup", this);
            this.canvas.removeEventListener("mousemove", this);
            this.canvas.removeEventListener("dblclick", this);
            window.removeEventListener("keydown", this);
            window.removeEventListener("keyup", this);
        }
        this.canvas = null;
    }
    handleEvent(e) {
        switch (e.type) {
            case "mousedown":
                this.onMouseDown(e);
                break;
            case "mouseup":
                this.onMouseUp(e);
                break;
            case "mousemove":
                this.onMouseMove(e);
                break;
            case "dblclick":
                this.onDoubleClick(e);
                break;
            case "keydown":
                this.onKeyDown(e);
                break;
            case "keyup":
                this.onKeyUp(e);
                break;
        }
    }
    onMouseDown(e) {
        this.operation.onMouseDown(e, this.modKeys);
    }
    onMouseUp(e) {
        this.operation.onMouseUp(e, this.modKeys);
    }
    onMouseMove(e) {
        this.operation.onMouseMove(e, this.modKeys);
    }
    onDoubleClick(e) {
        this.operation.onDoubleClick(e, this.modKeys);
    }
    onKeyDown(e) {
        if (e.key === MODIFIER_KEYS.SHIFT) {
            this.modKeys.shift = true;
        } else if (e.key === MODIFIER_KEYS.CTRL) {
            this.modKeys.ctrl = true;
        } else if (e.key === MODIFIER_KEYS.ALT) {
            this.modKeys.alt = true;
        } else {
            this.keys[e.key] = true;
        }
        this.operation.onKeyDown(e, this.modKeys);
    }
    onKeyUp(e) {
        if (e.key === MODIFIER_KEYS.SHIFT) {
            this.modKeys.shift = false;
        } else if (e.key === MODIFIER_KEYS.CTRL) {
            this.modKeys.ctrl = false;
        } else if (e.key === MODIFIER_KEYS.ALT) {
            this.modKeys.alt = false;
        } else {
            this.keys[e.key] = false;
        }
        this.operation.onKeyUp(e, this.modKeys);
    }
}
