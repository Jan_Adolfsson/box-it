import * as MOUSE_BUTTONS from "../../../../mouseButtons.js";
import { getCell, getSelectionAt } from "../../../../boxsheet.js";
import {
    createPt,
    polysContainingPt,
    polyClosestToPt,
    rectContainsPoly
}
    from "../../../../shapes/poly.js";
import { Operation } from "../operation.js";
import { SelectPolyOp } from "./selectPolyOp.js";
import { SelectContentOp } from "./selectContentOp.js";


export class IdleSelectionOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb) {
        super(cmdMgr, cmdFty, painter, redrawCb);
    }

    onMouseDown(e, modKeys) {
        const cell = getCell(this.painter.cells, this.painter.index);
        if (cell.polys.length === 0) {
            return;
        }

        const pt = createPt(e.offsetX, e.offsetY, this.painter.zoom);
        const polys = polysContainingPt(cell.polys, pt);
        const selection = getSelectionAt(this.painter.cells, this.painter.index);
        const segHit = polyClosestToPt(polys, pt, Infinity);

        if (e.button === MOUSE_BUTTONS.SECOND) {
            if (selection.length > 0) {
                if (segHit) {
                    if (selection.includes(segHit.poly)) {
                        return;
                    }
                }
                if (this.painter.selectionRect.contains(pt)) {
                    return;
                }
            }
        }
        if (polys.length > 0) {
            this.selectPolyOp(e, modKeys, segHit.poly);
        } else {
            this.selectContentOp(e, modKeys);
        }
    }

    onMouseUp(e, modKeys) {
    }

    onMouseMove(e, modKeys) {
    }

    selectPolyOp(e, modKeys, closest) {
        const op = new SelectPolyOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb, closest);
        this.painter.tool.setOperation(op);
        this.painter.tool.operation.onMouseDown(e, modKeys);
    }

    selectContentOp(e, modKeys) {
        const op = new SelectContentOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb);
        this.painter.tool.setOperation(op);
        this.painter.tool.operation.onMouseDown(e, modKeys);
    }
}