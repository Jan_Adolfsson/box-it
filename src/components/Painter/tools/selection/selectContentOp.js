import { Operation } from "../operation.js";
import { IdleSelectionOp } from "./idleSelectionOp.js";
import { CmdGroup } from "../../../../commands/CmdGroup.js";
import { getCell } from "../../../../boxsheet.js";
import { SelectionRect } from "../../../../selectionRect.js";
import {
    rectContainsPoly
} from "../../../../shapes/poly.js";

const SelectionType = {
    All: 0,
    Selected: 1,
    Unselected: 2
};
export class SelectContentOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb) {
        super(cmdMgr, cmdFty, painter, redrawCb);
        this.mouseSrcPos = null;
        this.painter = painter;
    }

    abort() {
        // Don't think it's needed.
    }

    onMouseDown(e, modKeys) {
        this.mouseSrcPos = { x: e.offsetX, y: e.offsetY };
    }

    onMouseUp(e, modKeys) {
        let selType = null;

        if (!modKeys.shift && !modKeys.ctrl) {
            selType = SelectionType.All;
        } else if (modKeys.shift) {
            selType = SelectionType.Unselected;
        } else if (modKeys.ctrl) {
            selType = SelectionType.Selected;
        }
        const hits = this.getSelectedContent(selType);

        if (hits.polys.lenght >= 0 || hits.pts.length >= 0) {
            let cmdGrp;
            if (modKeys.shift) {
                cmdGrp = this.addToSelection(hits);
            } else if (modKeys.ctrl) {
                cmdGrp = this.removeFromSelection(hits);
            } else {
                cmdGrp = this.newSelection(hits);
            }
            this.painter.selectionRect.clear();
            console.log(cmdGrp);
            this.cmdMgr.add(cmdGrp);
            this.cmdMgr.peek().do();
        }
        //this.clearSelectionRect();
        this.redrawCb();

        this.painter.tool.setOperation(this.idleSelectionOp());
    }

    getSelectedContent(selectionType) {
        const hits = {
            polys: [],
            pts: []
        };

        const cell = getCell(this.painter.cells, this.painter.index);
        cell.polys.forEach(poly => {
            if (rectContainsPoly(this.painter.selectionRect, poly)) {
                if ((poly.selected && (selectionType === SelectionType.Selected || selectionType === SelectionType.All)) ||
                    (!poly.selected && (selectionType === SelectionType.Unselected || selectionType === SelectionType.All))) {
                    hits.polys.push(poly);
                    poly.vertexes.filter(pt => {
                        return (pt.selected && (selectionType === SelectionType.Selected || selectionType === SelectionType.All)) ||
                            (!pt.selected && (selectionType === SelectionType.Unselected || selectionType === SelectionType.All));
                    }).forEach(pt =>
                        hits.pts.push(pt)
                    );
                } else {
                    poly.vertexes.filter(pt => {
                        return (pt.selected && (selectionType === SelectionType.Selected || selectionType === SelectionType.All)) ||
                            (!pt.selected && (selectionType === SelectionType.Unselected || selectionType === SelectionType.All));
                    }).forEach(pt => {
                        if (this.painter.selectionRect.contains(pt)) {
                            hits.pts.push(pt);
                        }
                    });
                }
            } else {
                poly.vertexes.filter(pt => {
                    return (pt.selected && (selectionType === SelectionType.Selected || selectionType === SelectionType.All)) ||
                        (!pt.selected && (selectionType === SelectionType.Unselected || selectionType === SelectionType.All));
                }).forEach(pt => {
                    if (this.painter.selectionRect.contains(pt)) {
                        hits.pts.push(pt);
                    }
                });
            }
        });

        return hits;
    }

    addToSelection(hits) {
        const cmdGrp = new CmdGroup();

        hits.pts.forEach(pt => {
            cmdGrp.add(this.cmdFty.selectPt(pt));
        })

        hits.polys.forEach(poly => {
            cmdGrp.add(this.cmdFty.selectPoly(poly));
        })

        return cmdGrp;
    }

    removeFromSelection(hits) {
        const cmdGrp = new CmdGroup();

        hits.pts.forEach(pt => {
            cmdGrp.add(this.cmdFty.unselectPt(pt));
        })

        hits.polys.forEach(poly => {
            cmdGrp.add(this.cmdFty.unselectPoly(poly));
        })

        return cmdGrp;
    }

    newSelection(hits) {
        console.log("New selection");
        const cmdGrp = new CmdGroup();
        cmdGrp.add(this.cmdFty.unselectAll(this.painter.cells));

        hits.pts.forEach(pt => {
            cmdGrp.add(this.cmdFty.selectPt(pt));
        })

        hits.polys.forEach(poly => {
            cmdGrp.add(this.cmdFty.selectPoly(poly));
        })

        return cmdGrp;
    }


    onMouseMove(e, modKeys) {
        this.select(modKeys, { x: e.offsetX, y: e.offsetY });
    }

    select(modKeys, mousePos) {
        const rect = this.createSelectionRect(this.mouseSrcPos, mousePos)
        this.painter.selectionRect.x = rect.x;
        this.painter.selectionRect.y = rect.y;
        this.painter.selectionRect.width = rect.width;
        this.painter.selectionRect.height = rect.height;

        this.redrawCb();
    }

    createSelectionRect(startPos, endPos) {
        const x = startPos.x <= endPos.x ? startPos.x : endPos.x;
        const y = startPos.y <= endPos.y ? startPos.y : endPos.y;
        const w = Math.abs(endPos.x - startPos.x);
        const h = Math.abs(endPos.y - startPos.y);

        return new SelectionRect(
            x / this.painter.zoom,
            y / this.painter.zoom,
            w / this.painter.zoom,
            h / this.painter.zoom
        );
    }

    idleSelectionOp() {
        return new IdleSelectionOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb);
    }
}