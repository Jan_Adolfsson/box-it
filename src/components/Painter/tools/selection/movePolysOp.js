import { Operation } from "../operation.js";
import { IdleSelectionOp } from "./idleSelectionOp.js";
import { getCell } from "../../../../boxsheet.js";

export class MovePolysOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb, mouseSrcPos) {
        super(cmdMgr, cmdFty, painter, redrawCb);
        this.mouseSrcPos = mouseSrcPos;
        this.lastMousePos = { x: this.mouseSrcPos.x, y: this.mouseSrcPos.y };
    }

    onMouseDown(e) {
    }

    onMouseUp(e) {
        this.cmdMgr.add(this.cmdFty.offsetPts(this.getPtsOfSelectedPolys(), this.createOffset(e, this.mouseSrcPos)));

        this.painter.tool.setOperation(this.idleSelectionOp());
    }

    onMouseMove(e) {
        this.movePts(this.getPtsOfSelectedPolys(), this.createOffset(e, this.lastMousePos));
        this.updateLastMousePos(e);

        this.redrawCb();
    }

    getPtsOfSelectedPolys() {
        if (!this.sltPts) {
            const cell = getCell(this.painter.cells, this.painter.index);
            this.sltPts = cell.polys.filter(p => p.selected).flatMap(p => p.vertexes);
        }
        return this.sltPts;
    }

    createOffset(e, srcPos) {
        return {
            x: (e.offsetX - srcPos.x) / this.painter.zoom,
            y: (e.offsetY - srcPos.y) / this.painter.zoom
        };
    }

    movePts(pts, offset) {
        pts.forEach(p => {
            p.x += offset.x;
            p.y += offset.y;
        })
    }

    updateLastMousePos(e) {
        this.lastMousePos.x = e.offsetX;
        this.lastMousePos.y = e.offsetY;
    }

    idleSelectionOp() {
        return new IdleSelectionOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb);
    }
}
