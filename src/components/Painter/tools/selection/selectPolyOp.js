import { Operation } from "../operation.js";
import { IdleSelectionOp } from "./idleSelectionOp.js";
import { MovePolysOp } from "./movePolysOp.js";
import { CmdGroup } from "../../../../commands/CmdGroup.js";

export class SelectPolyOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb, poly) {
        super(cmdMgr, cmdFty, painter, redrawCb);
        this.poly = poly;
        this.mouseSrcPos = null;
    }

    abort() {
        // Don't think it's needed.
    }

    onMouseDown(e, modKeys) {
        this.mouseSrcPos = { x: e.offsetX, y: e.offsetY };

        this.select(modKeys);
    }

    onMouseUp(e, modKeys) {
        this.painter.tool.setOperation(this.idleSelectionOp());
    }

    onMouseMove(e, modKeys) {
        this.select(modKeys);

        this.painter.tool.setOperation(this.movePolysOp());
        this.painter.tool.operation.onMouseMove(e, modKeys);
    }

    select(modKeys) {
        if (modKeys.shift) {
            this.cmdMgr.add(this.cmdFty.selectPoly(this.poly));
        } else {
            const cmdGrp = new CmdGroup();
            cmdGrp.add(this.cmdFty.unselectAll(this.painter.cells));
            cmdGrp.add(this.cmdFty.selectPoly(this.poly));
            this.cmdMgr.add(cmdGrp);
        }

        this.cmdMgr.peek().do();
    }

    idleSelectionOp() {
        return new IdleSelectionOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb);
    }

    movePolysOp() {
        return new MovePolysOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb, this.mouseSrcPos);
    }
}