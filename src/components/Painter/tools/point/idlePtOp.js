import { Operation } from "../operation.js";
import { SelectPtOp } from "./selectPtOp.js";
import { InsertPtOp } from "./insertPtOp.js";

export class IdlePtOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb) {
        super(cmdMgr, cmdFty, painter, redrawCb);
    }

    onMouseDown(e, modKeys) {
        this.painter.tool.setOperation(new SelectPtOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb));
        this.painter.tool.operation.onMouseDown(e, modKeys);
    }

    onDoubleClick(e, modKeys) {
        this.painter.tool.setOperation(new InsertPtOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb));
        this.painter.tool.operation.onDoubleClick(e, modKeys);
    }
}