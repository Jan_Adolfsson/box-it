import { getCell } from "../../../../boxsheet.js";
import {
    createPt,
    polyClosestToPt
}
    from "../../../../shapes/poly.js";
import { Operation } from "../operation.js";
import { IdlePtOp } from "./idlePtOp.js";
import { CmdGroup } from "../../../../commands/CmdGroup.js";

const POINT_HIT_TEST_DISTANCE = 5;

export class InsertPtOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb) {
        super(cmdMgr, cmdFty, painter, redrawCb);
    }
    onDoubleClick(e, modKeys) {
        const cell = getCell(this.painter.cells, this.painter.index);
        const polys = cell.polys;
        if (polys.length === 0) {
            return;
        }

        const pt = createPt(e.offsetX, e.offsetY, this.painter.zoom);
        const hit = polyClosestToPt(polys, pt, POINT_HIT_TEST_DISTANCE);
        if (!hit) {
            return;
        }

        const cmdGroup = new CmdGroup();
        cmdGroup.add(this.cmdFty.insertPt(hit.poly, hit.pt, hit.segment));
        cmdGroup.add(this.cmdFty.selectPt(hit.pt));
        this.cmdMgr.add(cmdGroup);
        this.cmdMgr.peek().do();

        this.painter.tool.setOperation(new IdlePtOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb));
    }
}
