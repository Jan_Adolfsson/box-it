import { Operation } from "../operation.js";
import { IdlePtOp } from "./idlePtOp.js";
import {
    offsetPts,
    filterPts
} from "../../../../shapes/poly.js";
import { getCell } from "../../../../boxsheet.js";

export class MovePtsOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb, mouseSrcPos) {
        super(cmdMgr, cmdFty, painter, redrawCb);
        this.mouseSrcPos = mouseSrcPos;
        this.lastMousePos = { x: this.mouseSrcPos.x, y: this.mouseSrcPos.y };
    }

    onMouseUp(e, modKeys) {
        this.cmdMgr.add(this.cmdFty.offsetPts(this.getSelectedPts(), this.createOffset(e, this.mouseSrcPos)));
        this.painter.tool.setOperation(new IdlePtOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb));
    }

    onMouseMove(e, modKeys) {
        const pts = this.getSelectedPts();
        const offset = this.createOffset(e, this.lastMousePos);
        offsetPts(pts, offset);

        this.updateLastMousePos(e);

        this.redrawCb();
    }

    getSelectedPts() {
        if (!this.sltPts) {
            const cell = getCell(this.painter.cells, this.painter.index);
            this.sltPts = filterPts(cell.polys, pt => pt.selected);
        }
        return this.sltPts;
    }

    createOffset(e, srcPos) {
        return {
            x: (e.offsetX - srcPos.x) / this.painter.zoom,
            y: (e.offsetY - srcPos.y) / this.painter.zoom
        };
    }

    updateLastMousePos(e) {
        this.lastMousePos.x = e.offsetX;
        this.lastMousePos.y = e.offsetY;
    }
}
