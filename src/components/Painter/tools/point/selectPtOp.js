import { getCell } from "../../../../boxsheet.js";
import {
    createPt,
    polyClosestToPt,
    polysWithVtxsCloseToPt,
    hitWithVtxClosestToPt
}
    from "../../../../shapes/poly.js";
import { Operation } from "../operation.js";
import { IdlePtOp } from "./idlePtOp.js";
import { MovePtsOp } from "./movePtsOp.js";
import { CmdGroup } from "../../../../commands/CmdGroup.js";

const POINT_HIT_TEST_DISTANCE = 5;

export class SelectPtOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb) {
        super(cmdMgr, cmdFty, painter, redrawCb);
        this.mouseSrcPos = null;
    }
    onMouseDown(e, modKeys) {
        this.mouseSrcPos = { x: e.offsetX, y: e.offsetY };

        const cell = getCell(this.painter.cells, this.painter.index);
        const polys = cell.polys;
        if (polys.length === 0) {
            return;
        }

        const pt = createPt(e.offsetX, e.offsetY, this.painter.zoom);
        const hits = polysWithVtxsCloseToPt(polys, pt, POINT_HIT_TEST_DISTANCE);
        if (hits.length === 0) {
            return;
        }

        const hit = hitWithVtxClosestToPt(hits, pt);

        if (modKeys.shift) {
            this.multiSelect(hit.pt);
        } else {
            this.singleSelect(hit.pt);
        }

        this.redrawCb();
    }

    singleSelect(pt) {
        const cmdGrp = new CmdGroup();
        cmdGrp.add(this.cmdFty.unselectAll(this.painter.cells));
        const cmd = pt.selected
            ? this.cmdFty.unselectPt(pt)
            : this.cmdFty.selectPt(pt);
        cmdGrp.add(cmd);
        this.cmdMgr.add(cmdGrp);
        this.cmdMgr.peek().do();
    }
    multiSelect(pt) {
        const cmd = pt.selected
            ? this.cmdFty.unselectPt(pt)
            : this.cmdFty.selectPt(pt)
        this.cmdMgr.add(cmd);
        this.cmdMgr.peek().do();
    }

    onMouseUp(e, modKeys) {
        this.painter.tool.setOperation(new IdlePtOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb));
    }

    onMouseMove(e, modKeys) {
        this.painter.tool.setOperation(new MovePtsOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb, this.mouseSrcPos));
        this.painter.tool.operation.onMouseMove(e, modKeys, this.mouseSrcPos);
    }
}
