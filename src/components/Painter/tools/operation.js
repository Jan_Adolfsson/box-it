import { getCell } from "../../../boxsheet";
import { CmdGroup } from "../../../commands/CmdGroup";

export class Operation {
    constructor(cmdMgr, cmdFty, painterInfo, redrawCb) {
        this.cmdMgr = cmdMgr;
        this.cmdFty = cmdFty;
        this.painter = painterInfo;
        this.redrawCb = redrawCb;
    }
    setCells(cells) {
        this.painter.cells = cells;
    }
    setIndex(index) {
        this.painter.index = index;
    }
    getPainterInfo() {
        return this.painter;
    }
    setPainterInfo(painterInfo) {
        this.painter = painterInfo;
    }
    setTool(tool) {
        this.painter.tool = tool;
    }
    setZoom(zoom) {
        this.painter.zoom = zoom;
    }
    abort() { }
    finalize() { }
    onContentChange(callback) {
        this.redrawCb = callback;
    }
    onMouseDown(e, modKeys) { }
    onMouseUp(e, modKeys) { }
    onMouseMove(e, modKeys) { }
    onDoubleClick(e, modKeys) { }
    onKeyDown(e, modKeys) {
        if (e.code === "Backspace" || e.code === "Delete") {
            const cell = getCell(this.painter.cells, this.painter.index);
            const polysToRmv = []
            const ptsToRmv = [];
            cell.polys.forEach(poly => {
                if (poly.selected) {
                    polysToRmv.push(poly);
                } else {
                    const ptIdxPairs = poly.vertexes
                        .map((p, i) => { return { pt: p, index: i } })
                        .filter(p => p.pt.selected);
                    if (ptIdxPairs.length > 0 && poly.vertexes.length - ptIdxPairs.length > 2) {
                        ptsToRmv.push({
                            poly: poly,
                            pointIndexPairs: ptIdxPairs
                        });
                    }
                }
            });

            const cmdGroup = new CmdGroup();
            cmdGroup.add(this.cmdFty.removePolys(cell, polysToRmv));
            ptsToRmv.forEach(p => cmdGroup.add(this.cmdFty.removePts(p.poly, p.pointIndexPairs)));
            this.cmdMgr.add(cmdGroup);
            this.cmdMgr.peek().do();
        } else if (e.code === "Escape") {
            this.abort();
        } else if (e.code === "Enter") {
            this.finalize();
        }
    }
    onKeyUp(e, modKeys) {

    }
}