import {
    boundingBoxOfPoly,
    boundingBoxOfPolyScaled,
    offsetPoly,
    scalePoly,
    areaOfPoly,
    POLY_H_AREA,
    POLY_V_AREA
}
    from "../../../../shapes/poly.js";
import { Operation } from "../operation.js";
import { IdleResizeOp } from "./idleResizeOp.js";

const POLY_MIN_WIDTH = 1;
const POLY_MIN_HEIGHT = 1;

export class ResizePolysOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb, polys, closest) {
        super(cmdMgr, cmdFty, painter, redrawCb);
        this.polys = polys;
        this.closest = closest;
        this.lastMousePos = null;
        this.mouseStartPos = null;
        this.oldPts = [];
        this.offset = {
            x: 0,
            y: 0
        };
        this.origArea = null;
    }

    onMouseDown(e, modKeys) {
        this.mouseStartPos = {
            x: e.offsetX,
            y: e.offsetY
        };

        const polysPts = this.polys.map(poly => poly.vertexes);
        this.cmdMgr.add(this.cmdFty.resizePolys(polysPts, this.offset));

        this.origArea = areaOfPoly(this.closest.poly, this.closest.pt);
    }

    onMouseUp(e, modKeys) {
        this.offset.x = this.lastMousePos.x - this.mouseStartPos.x;
        this.offset.y = this.lastMousePos.y - this.mouseStartPos.y;

        const op = new IdleResizeOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb);
        this.painter.tool.setOperation(op);
    }

    onMouseMove(e, modKeys) {
        if (!this.lastMousePos) {
            this.lastMousePos = {
                x: this.mouseStartPos.x,
                y: this.mouseStartPos.y
            }
        }
        const box = boundingBoxOfPoly(this.closest.poly.vertexes);
        const delta = this.calcDelta(e);
        const scaleFact = this.calcScaleFact(box, delta);

        this.resizePolys(this.polys, scaleFact, this.origArea);

        this.lastMousePos.x = e.offsetX;
        this.lastMousePos.y = e.offsetY;

        this.redrawCb();
    }
    calcDelta(e) {
        return {
            x: calcDeltaX(this.origArea.x, this.lastMousePos.x, e.offsetX, this.painter.zoom),
            y: calcDeltaY(this.origArea.y, this.lastMousePos.y, e.offsetY, this.painter.zoom)
        };

        function calcDeltaX(hOrigArea, lastX, currX, zoom) {
            const d = hOrigArea === POLY_H_AREA.LEFT ? lastX - currX : currX - lastX;
            return d / zoom;
        }
        function calcDeltaY(vOrigArea, lastY, currY, zoom) {
            const d = vOrigArea === POLY_V_AREA.TOP ? lastY - currY : currY - lastY;
            return d / zoom;
        }
    }
    calcScaleFact(bbox, delta) {
        return {
            x: (bbox.width + delta.x) / bbox.width,
            y: (bbox.height + delta.y) / bbox.height
        };
    }
    resizePolys(polys, s, origArea) {
        for (const poly of polys) {
            const boxScaled = boundingBoxOfPolyScaled(poly.vertexes, s);
            if (boxScaled.width < POLY_MIN_WIDTH ||
                boxScaled.height < POLY_MIN_HEIGHT) {
                continue;
            }

            const oldBox = boundingBoxOfPoly(poly.vertexes);

            scalePoly(poly.vertexes, s);

            const newBox = boundingBoxOfPoly(poly.vertexes);
            const offset = calcOffset(oldBox, newBox, origArea);

            offsetPoly(poly, offset);
        }

        function calcOffset(oldBox, newBox, origArea) {
            return {
                x: calcHOffset(oldBox.width, newBox.width, origArea.x),
                y: calcVOffset(oldBox.height, newBox.height, origArea.y)
            };

            function calcHOffset(oldWidth, newWidth, hOrigArea) {
                let offs = newWidth - oldWidth;

                if (hOrigArea === POLY_H_AREA.LEFT) {
                    offs *= -1;
                }

                return offs / 2;
            }

            function calcVOffset(oldHeight, newHeight, vOrigArea) {
                let offs = newHeight - oldHeight;

                if (vOrigArea === POLY_V_AREA.TOP) {
                    offs *= -1;
                }

                return offs / 2;
            }
        }
    }
}