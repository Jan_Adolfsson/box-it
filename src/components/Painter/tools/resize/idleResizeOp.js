import { getCell } from "../../../../boxsheet.js";
import {
    hitWithVtxClosestToPt,
    createPt,
    polysWithVtxsCloseToPt
}
    from "../../../../shapes/poly.js";
import { Operation } from "../operation.js";
import { ResizePolysOp } from "./resizePolysOp.js";

const POINT_HIT_TEST_DISTANCE = 5;
export class IdleResizeOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb) {
        super(cmdMgr, cmdFty, painter, redrawCb);
    }

    onMouseDown(e, modKeys) {
        const cell = getCell(this.painter.cells, this.painter.index);
        const polys = cell.polys.filter(p => p.selected);
        if (polys.length === 0) {
            return;
        }

        const pt = createPt(e.offsetX, e.offsetY, this.painter.zoom);
        const hits = polysWithVtxsCloseToPt(polys, pt, POINT_HIT_TEST_DISTANCE);
        if (hits.length === 0) {
            return;
        }
        const closest = hitWithVtxClosestToPt(hits, pt);
        this.resizePolysOp(e, modKeys, polys, closest);

        //        if (polys.length === 0) {

        // } else {
        //     if (modKeys.ctrl) {
        //         const pt = createPt(e.offsetX, e.offsetY, this.painter.zoom);
        //         const hits = polysWithVtxsCloseToPt(polys, pt, POINT_HIT_TEST_DISTANCE);
        //         if (hits.length > 0) {
        //             const closestHit = hitWithVtxClosestToPt(hits, pt);
        //             this.movePolyPtOp(e, modKeys, closestHit);
        //         }
        //     } else {
        //         this.drawPolyOp(e, modKeys, cell);
        //     }
        // }
    }

    onMouseUp(e, modKeys) {
    }

    onMouseMove(e, modKeys) {
    }

    resizePolysOp(e, modKeys, polys, closest) {
        const op = new ResizePolysOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb, polys, closest);
        this.painter.tool.setOperation(op);
        this.painter.tool.operation.onMouseDown(e, modKeys);
    }
}