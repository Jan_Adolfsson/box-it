import { CmdGroup } from "../../../../commands/CmdGroup.js";
import { Operation } from "../operation.js";
import { IdleRectOp } from "./idleRectOp.js";
import { resizeByCorner } from "../../../../shapes/rect.js";

export class ResizeRectOperation extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb, hit) {
        super(cmdMgr, cmdFty, painter, redrawCb)
        this.hit = hit;
        this.ptIndex = null;
        this.oldPts = null;
        this.cmdBatch = null;
    }

    abort() {
        this.painter.tool.setOperation(this.idleRectOp());
    }
    onMouseDown(e) {
        this.cmdBatch = new CmdGroup();
        this.cmdMgr.add(this.cmdBatch);

        this.oldPts = JSON.parse(JSON.stringify(this.hit.poly.vertexes));

        const cmdGrp = new CmdGroup();
        cmdGrp.add(this.cmdFty.unselectAll(this.painter.cells));
        cmdGrp.add(this.cmdFty.selectPoly(this.hit.poly));
        cmdGrp.add(this.cmdFty.selectPt(this.hit.pt));
        cmdGrp.do();

        this.cmdBatch.add(cmdGrp);

        this.ptIndex = this.hit.poly.vertexes.indexOf(this.hit.pt);
    }

    onMouseUp(e) {
        this.cmdBatch.add(this.cmdFty.resizeRect(this.hit.poly, this.oldPts
        ));

        this.painter.tool.setOperation(this.idleRectOp());
    }
    onMouseMove(e) {
        resizeByCorner(this.hit.poly.vertexes, this.ptIndex, e.offsetX, e.offsetY, this.painter.zoom);

        if (this.redrawCb) {
            this.redrawCb();
        }
    }
    idleRectOp() {
        return new IdleRectOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb);
    }
}