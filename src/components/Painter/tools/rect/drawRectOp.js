import { CmdGroup } from "../../../../commands/CmdGroup.js";
import { createRect } from "../../../../shapes/poly.js";
import { Operation } from "../operation.js";
import { IdleRectOp } from "./idleRectOp.js";

export class DrawRectOperation extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb, cell) {
        super(cmdMgr, cmdFty, painter, redrawCb);
        this.cell = cell;
        this.rect = null;
    }
    abort() {
        this.cmdMgr.pop().undo();
        this.painter.tool.setOperation(this.idleRectOp());
    }
    finalize() {
        this.painter.tool.setOperation(this.idleRectOp());
    }
    onMouseDown(e, modKeys) {
        this.rect = createRect(this.painter.boxType, e.offsetX, e.offsetY, this.painter.zoom, false);

        const cmdGrp = new CmdGroup();
        this.cmdMgr.add(cmdGrp);

        cmdGrp.add(this.cmdFty.unselectAll(this.painter.cells))
        cmdGrp.add(this.cmdFty.addPoly(this.cell, this.rect));
        cmdGrp.add(this.cmdFty.selectPoly(this.rect));
        cmdGrp.add(this.cmdFty.selectPt(this.rect.vertexes[2]));

        cmdGrp.do();
    }

    onMouseUp(e, modKeys) {
        this.finalize();
    }
    onMouseMove(e, modKeys) {
        const vertexes = this.rect.vertexes;
        vertexes[1].x = e.offsetX / this.painter.zoom;
        vertexes[1].y = vertexes[0].y;

        vertexes[2].x = e.offsetX / this.painter.zoom;
        vertexes[2].y = e.offsetY / this.painter.zoom;

        vertexes[3].x = vertexes[0].x;
        vertexes[3].y = e.offsetY / this.painter.zoom;

        if (this.redrawCb) {
            this.redrawCb();
        }
    }
    idleRectOp() {
        return new IdleRectOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb);
    }
}