import { Operation } from "../operation.js";
import { IdleRectOp } from "./idleRectOp.js";
import { CmdGroup } from "../../../../commands/CmdGroup.js";

export class SelectRectOperation extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb, rect) {
        super(cmdMgr, cmdFty, painter, redrawCb);
        this.rect = rect;
    }

    abort() {
        // Don't think it's needed.
    }
    onMouseDown(e, modKeys) {
        if (modKeys.shift) {
            this.cmdMgr.add(this.cmdFty.selectPoly(this.rect));
        } else {
            const cmdGrp = new CmdGroup();
            cmdGrp.add(this.cmdFty.unselectAll(this.painter.cells));
            cmdGrp.add(this.cmdFty.selectPoly(this.rect));
            this.cmdMgr.add(cmdGrp);
        }

        this.cmdMgr.peek().do();
    }
    onMouseUp(e, modKeys) {
        this.painter.tool.setOperation(this.idleRectOp());
    }
    onMouseMove(e, modKeys) {
    }
    idleRectOp() {
        return new IdleRectOp(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb)
    }
}