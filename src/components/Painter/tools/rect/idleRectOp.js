import {
    createPt,
    isRect,
    polysWithVtxsCloseToPt,
    hitWithVtxClosestToPt,
    polysContainingPt
} from "../../../../shapes/poly.js";
import { getCell } from "../../../../boxsheet.js";
import { Operation } from "../operation.js";
import { ResizeRectOperation } from "./resizeRectOp";
import { SelectRectOperation } from "./selectRectOp";
import { DrawRectOperation } from "./drawRectOp";
import { rectClosestToPt } from "../../../../shapes/rect.js";

const POINT_HIT_TEST_DISTANCE = 5;

export class IdleRectOp extends Operation {
    constructor(cmdMgr, cmdFty, painter, redrawCb) {
        super(cmdMgr, cmdFty, painter, redrawCb);
    }

    onMouseDown(e, modKeys) {
        const cell = getCell(this.painter.cells, this.painter.index);
        const rects = cell.polys.filter(p => isRect(p));
        if (rects.length === 0) {
            this.painter.tool.setOperation(this.drawRectOp(cell));
            this.painter.tool.operation.onMouseDown(e, modKeys);
        } else {

            if (modKeys.ctrl) {
                const pt = createPt(e.offsetX, e.offsetY, this.painter.zoom);
                const hits = polysWithVtxsCloseToPt(rects, pt, POINT_HIT_TEST_DISTANCE);
                if (hits.length > 0) {
                    const closestHit = hitWithVtxClosestToPt(hits, pt);
                    this.painter.tool.setOperation(this.resizeRectOp(closestHit));
                    this.painter.tool.operation.onMouseDown(e, modKeys);
                } else {
                    const rectsContainingPt = polysContainingPt(rects, pt);
                    if (rectsContainingPt.length > 0) {
                        const rectContainingPt = rectClosestToPt(rectsContainingPt, pt);
                        this.painter.tool.setOperation(this.selectRectOp(rectContainingPt));
                        this.painter.tool.onMouseDown(e, modKeys);
                    }
                }
            } else {
                this.painter.tool.setOperation(this.drawRectOp(cell));
                this.painter.tool.operation.onMouseDown(e, modKeys);
            }
        }
    }
    onMouseUp(e, modKeys) {
    }
    onMouseMove(e, modKeys) {
    }
    drawRectOp(cell) {
        return new DrawRectOperation(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb, cell);
    }
    resizeRectOp(hit) {
        return new ResizeRectOperation(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb, hit);
    }
    selectRectOp(rect) {
        return new SelectRectOperation(this.cmdMgr, this.cmdFty, this.painter, this.redrawCb, rect);
    }
}