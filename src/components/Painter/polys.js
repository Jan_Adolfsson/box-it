export const addTagToPolys = (polys, tagId) => {
    polys.forEach(poly => {
        const tags = poly.tags;
        if (!tags.includes(tagId)) {
            tags.push(tagId);
        }
    });
}

export const removeTagFromPolys = (polys, tagId) => {
    polys.forEach(poly => {
        const tags = poly.tags;
        if (tags.includes(tagId)) {
            const idx = tags.indexOf(tagId);
            tags.splice(idx, 1);
        }
    });
}