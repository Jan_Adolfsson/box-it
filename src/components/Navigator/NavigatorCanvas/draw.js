import { toRGBA, toRGBAString, toAdjustedRGBA } from "../../../color";
import { drawPolys } from "../../../polyGrfx";

const FOCUS_RECT_LINE_WIDTH = 2;
const SELECTION_ALPHA = 0.25;
const GRIP_SIZE = 8;
const POLY_LINE_WIDTH = 1;
const POLY_SELECTED_LINE_WIDTH = 2;


function tryDraw(drawCtx, motif, appearance, interaction) {
    if (!motif.spritesheet) {
        console.log("spritesheet is not set. Abandon tryDraw.")
        return;
    }

    const zoom = interaction.zoom / 100;

    resizeCanvas(drawCtx.canvas, calcCanvasSize(motif.spritesheet, zoom));
    fillBackground(drawCtx, appearance.canvas.bgColor);
    drawSpritesheet(drawCtx, motif.spritesheet, zoom);

    const cellSize = {
        x: motif.spriteSize.x * zoom,
        y: motif.spriteSize.y * zoom
    };

    const cellAppearance = {
        lineWidth: POLY_LINE_WIDTH,
        selectedLineWidth: POLY_SELECTED_LINE_WIDTH,
        showGrip: false,
        boxTypes: appearance.boxTypes,
        defaultColor: appearance.defaultColor,
        size: motif.spriteSize,
        showTags: true,
        tags: appearance.tags
    };

    drawCells(drawCtx, motif.cells, cellAppearance, zoom);

    if (appearance.canvas.showGrid) {
        tryDrawGrid(drawCtx, motif.spritesheetSize, cellSize, toRGBA(appearance.canvas.gridColor));
    }

    const adjustedSelectionClr = toAdjustedRGBA(appearance.selection.fillColor, SELECTION_ALPHA);
    if (interaction.selection.length > 0) {
        drawSelection(drawCtx, interaction.selection, cellSize, adjustedSelectionClr);
    }

    if (interaction.index.x > -1) {
        drawFocusRect(drawCtx, interaction.index, cellSize, toRGBA(appearance.selection.lineColor), FOCUS_RECT_LINE_WIDTH);
    }

    if (interaction.selectionRect) {
        drawMouseSelectionRect(drawCtx, interaction.selectionRect, cellSize, adjustedSelectionClr);
    }
}

function drawCells(drawCtx, cells, cellAppearance, zoom) {
    cells.forEach(cell => {
        drawPolys(drawCtx, cell.polys, cellAppearance, zoom, calcOffset(cell.index, cellAppearance.size))
    });

    function calcOffset(index, size) {
        return {
            x: index.x * size.x,
            y: index.y * size.y
        };
    }
}

function calcCanvasSize(spritesheet, zoom) {
    return {
        width: spritesheet.width * zoom,
        height: spritesheet.height * zoom
    };
}
function resizeCanvas(canvas, size) {
    canvas.width = size.width;
    canvas.height = size.height;
    canvas.style.width = size.width;
    canvas.style.height = size.height;
}
function drawSelection(drawCtx, selection, cellSize, color) {
    selection.forEach(idx => {
        fillSelectionRect(drawCtx, idx, cellSize, color);
    })
}
function drawMouseSelectionRect(drawCtx, selectionRect, cellSize, color) {
    const rect = {
        x: selectionRect.x * cellSize.x,
        y: selectionRect.y * cellSize.y,
        width: selectionRect.width * cellSize.x,
        height: selectionRect.height * cellSize.y
    };

    drawCtx.ctx.fillStyle = toRGBAString(color);
    drawCtx.ctx.fillRect(rect.x, rect.y, rect.width, rect.height);
}
function drawFocusRect(drawCtx, index, cellSize, color, lineWidth) {
    const pos = {
        x: index.x * cellSize.x,
        y: index.y * cellSize.y,
    };

    drawCtx.ctx.strokeStyle = toRGBAString(color);
    drawCtx.ctx.lineWidth = lineWidth;
    drawCtx.ctx.strokeRect(pos.x, pos.y, cellSize.x, cellSize.y);
}
function fillSelectionRect(drawCtx, index, cellSize, color) {
    const pos = {
        x: index.x * cellSize.x,
        y: index.y * cellSize.y,
    };

    drawCtx.ctx.fillStyle = toRGBAString(color);
    drawCtx.ctx.fillRect(pos.x, pos.y, cellSize.x, cellSize.y);
}
function fillBackground(drawCtx, color) {
    drawCtx.ctx.fillStyle = color;
    drawCtx.ctx.fillRect(0, 0, drawCtx.canvas.width, drawCtx.canvas.height);
}
function drawSpritesheet(drawCtx, spritesheet, zoom) {
    drawCtx.ctx.drawImage(spritesheet, 0, 0, spritesheet.width * zoom, spritesheet.height * zoom);
}
function tryDrawGrid(drawCtx, spritesheetSize, spriteSize, gridColor) {
    if (!spritesheetSize) {
        console.log("spritesheetSize is not set. Abandon tryDraw.")
        return;
    }

    if (!spriteSize) {
        console.log("spriteSize is not set. Abandon tryDraw.")
        return;
    }

    if (!gridColor) {
        console.log("gridColor is not set. Abandon tryDraw.")
        return;
    }

    drawVerticalLines(drawCtx.ctx, spritesheetSize, spriteSize, gridColor);
    drawHorizontalLines(drawCtx.ctx, spritesheetSize, spriteSize, gridColor);
}
function drawVerticalLines(context, spritesheetSize, spriteSize, color) {
    for (let i = 0; i < spritesheetSize.x + 1; ++i) {
        context.beginPath();
        context.strokeStyle = toRGBAString(color);

        let x = i * spriteSize.x;
        let y0 = 0;
        let y1 = spritesheetSize.y * spriteSize.y;

        context.moveTo(x, y0);
        context.lineTo(x, y1);

        context.stroke()
    }
}
function drawHorizontalLines(context, spritesheetSize, spriteSize, color) {
    for (let i = 0; i < spritesheetSize.y + 1; ++i) {
        context.beginPath();
        context.strokeStyle = toRGBAString(color);

        let y = i * spriteSize.y;
        let x0 = 0;
        let x1 = spritesheetSize.x * spriteSize.x;

        context.moveTo(x0, y);
        context.lineTo(x1, y);

        context.stroke()
    }
}

function clearCanvas(drawCtx, bgColor) {
    // drawCtx.ctx.fillStyle = bgColor;
    // drawCtx.ctx.fillRect(0, 0, drawCtx.canvas.width, drawCtx.canvas.height);
    drawCtx.ctx.clearRect(0, 0, drawCtx.canvas.width, drawCtx.canvas.height);
}

export { tryDraw, clearCanvas };