const fs = require("fs");

export const loadNavigatorSettings = (path) => {
    console.log(`Load NavigatorSettings from path: ${path}`);

    const json = fs.readFileSync(path, { encoding: "utf-8" });
    const settings = JSON.parse(json);

    console.log(`Navigator settings loaded as ${json}`);
    return settings;
}

export const createDefaultNavigatorSettings = () => {
    return {
        canvas: {
            showGrid: true,
            gridColor: "#dedede",
            bgColor: "#ffffff"
        },
        selection: {
            lineColor: "#e8a527",
            fillColor: "#e8a527",
        },
        polygon: {
            fill: false,
            fillColor: "#00a2ff",
            defaultColor: "#00a2ff"
        },
        project: {
            spritesheetSize: { x: 8, y: 8 },
            spriteSize: { x: 32, y: 32 }
        },
    };
}

export const saveNavigatorSettings = (path, settings) => {
    const json = JSON.stringify(settings);

    console.log(`Save Navigator settings ${json} to path: ${path}`);

    fs.writeFileSync(path, json, { encoding: "utf-8" });
}