function coordinateToIndex(coordinate, spriteSize, zoom) {
    return {
        x: Math.trunc(coordinate.x / (zoom * spriteSize.x)),
        y: Math.trunc(coordinate.y / (zoom * spriteSize.y))
    }
}
function indexToCoordinate(index, spriteSize) {
    return {
        x: index.x * spriteSize.x,
        y: index.y * spriteSize.y,
    };
}
export { coordinateToIndex, indexToCoordinate };