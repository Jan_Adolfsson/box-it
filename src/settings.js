const fs = require("fs");

import { BOX_TYPES } from "./constants.js";

export const DEFAULT_PROJ_NAME = "sampleProj.json";
export const DEFAULT_BOX_COLORS = {
    HIT: "#b23c42",
    HURT: "#c1923f",
    BLOCK: "#2f7f54",
    TRIGGER: "#28609b",
    OTHER: "#962a91"
};

export function loadSettings(path) {
    console.log(`Load settings from path: ${path}`);

    const json = fs.readFileSync(path, { encoding: "utf-8" });
    const settings = JSON.parse(json);

    for (let i = 0; i < settings.boxTypes.length; ++i) {
        settings.boxTypes[i].id = i;
    }

    for (let i = 0; i < settings.tags.length; ++i) {
        settings.tags[i].id = i;
    }

    console.log(`Settings loaded as ${json}`);
    return settings;
}

export const saveSettings = (path, settings) => {
    const json = JSON.stringify(settings);
    console.log(`Save settings ${json} to path: ${path}`);

    fs.writeFileSync(path, json, { encoding: "utf-8" });
}

export function createDefaultSettings() {
    return {
        loadProjectAtStart: true,
        projectPath: DEFAULT_PROJ_NAME,
        boxTypes: [
            {
                type: BOX_TYPES.HIT,
                color: DEFAULT_BOX_COLORS.HIT
            },
            {
                type: BOX_TYPES.HURT,
                color: DEFAULT_BOX_COLORS.HURT
            },
            {
                type: BOX_TYPES.BLOCK,
                color: DEFAULT_BOX_COLORS.BLOCK
            },
            {
                type: BOX_TYPES.TRIGGER,
                color: DEFAULT_BOX_COLORS.TRIGGER
            },
            {
                type: BOX_TYPES.OTHER,
                color: DEFAULT_BOX_COLORS.OTHER
            }
        ],
        defaultBoxType: BOX_TYPES.HURT,
        tags: []
    };
}