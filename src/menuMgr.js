const remote = require("electron").remote;

const CMD_OR_CTRL = "CmdOrCtrl";
const SHIFT = "Shift"
const CTRL = "Ctrl";
const ROLE = {
    RELOAD: "reload",
    DEVTOOLS: "toggleDevTools",
    FULLSCREEN: "toggleFullscreen",
    SELECT_ALL: "selectAll",
    QUIT: "quit"
};
const ITEM_TYPES = {
    NORMAL: "normal",
    RADIO: "radio",
    SEPARATOR: "separator"
};

export const MENU_IDS = {
    FILE: "FILE",
    FILE_NEW: "FILE_NEW",
    FILE_OPEN: "FILE_OPEN",
    FILE_SAVE: "FILE_SAVE",
    FILE_SAVE_AS: "FILE_SAVE_AS",
    FILE_SETTINGS: "FILE_SETTINGS",
    FILE_QUIT: "FILE_QUIT",

    NAVIGATOR: "NAVIGATOR",
    NAVIGATOR_EXPORT: "NAVIGATOR_EXPORT",
    NAVIGATOR_OPEN: "NAVIGATOR_OPEN",
    NAVIGATOR_CLOSE: "NAVIGATOR_CLOSE",
    NAVIGATOR_SETTINGS: "NAVIGATOR_SETTINGS",

    PAINTER: "PAINTER",
    PAINTER_EXPORT: "PAINTER_EXPORT",
    PAINTER_DRAW_POLY: "PAINTER_DRAW_POLY",
    PAINTER_DRAW_RECT: "PAINTER_DRAW_RECT",
    PAINTER_RESIZE: "PAINTER_RESIZE",
    PAINTER_SELECT: "PAINTER_SELECT",
    PAINTER_SETTINGS: "PAINTER_SETTINGS",
    PAINTER_TWEAK: "PAINTER_TWEAK"
}
export class MenuMgr {
    constructor() {
        this.onNewProject = null;
        this.onOpenProject = null;
        this.onSaveProject = null;
        this.onSaveProjectAs = null;
        this.onSettings = null;

        // EDIT ACTIONS
        this.onUndo = null;
        this.onRedo = null;
        this.onCut = null;
        this.onCopy = null;
        this.onPaste = null;
        this.onSelectAll = null;

        // PAINTER ACTIONS
        this.onPainterSelect = null;
        this.onPainterRect = null;
        this.onPainterPoly = null;
        this.onPainterTweak = null;
        this.onPainterResize = null;
        this.onPainterExport = null;
        this.onPainterSettings = null;

        // NAVIGATION ACTIONS
        this.onNavigatorOpen = null;
        this.onNavigatorClose = null;
        this.onNavigatorExport = null;
        this.onNavigatorSettings = null;

        // HELP ACTIONS
        this.onAbout = null;
        this.onReleaseNotes = null;
        this.onDocumentation = null;
    }

    build() {
        this.template = [
            this.file(),
            this.edit(),
            this.view(),
            this.painter(),
            this.navigator(),
            this.help()
        ];
    }

    file() {
        const file = {
            label: "&File",
            id: MENU_IDS.FILE,
            submenu: [
                normal("&New", MENU_IDS.FILE_NEW, [CMD_OR_CTRL, "N"], this.onNewProject),
                normal("&Open...", MENU_IDS.FILE_OPEN, [CMD_OR_CTRL, "O"], this.onOpenProject),
                separator(),
                normal("&Save", MENU_IDS.FILE_SAVE, [CMD_OR_CTRL, "S"], this.onSaveProject),
                normal("Save &As...", MENU_IDS.FILE_SAVE_AS, [CMD_OR_CTRL, SHIFT, "S"], this.onSaveProjectAs),
                separator(),
                normal("Settings...", MENU_IDS.FILE_SETTINGS, [CMD_OR_CTRL, "+"], this.onSettings),
                separator(),
                normalWithRole("&Quit", MENU_IDS.FILE_QUIT, [CMD_OR_CTRL, "Q"], ROLE.QUIT),
            ]
        };
        return file;
    }
    edit() {
        const edit = {
            label: "&Edit",
            id: "EDIT",
            submenu: [
                normal("&Undo", "EDIT_UNDO", [CMD_OR_CTRL, "Z"], this.onUndo),
                normal("&Redo", "EDIT_REDO", [CMD_OR_CTRL, "Y"], this.onRedo),
                separator(),
                normal("Cu&t", "EDIT_CUT", [CMD_OR_CTRL, "X"], this.onCut),
                normal("&Copy", "EDIT_COPY", [CMD_OR_CTRL, "C"], this.onCopy),
                normal("&Paste", "EDIT_PASTE", [CMD_OR_CTRL, "V"], this.onPaste),
                separator(),
                normal("&Select All", "EDIT_SELECT_ALL", [CMD_OR_CTRL, "A"], this.onSelectAll), // Setting up Ctrl+A doesn't work, see HACK below.
            ]
        };

        return edit;
    }

    view() {
        const view = {
            label: "&View",
            id: "VIEW",
            submenu: [
                normalWithRole("&Fullscreen", "VIEW_FULLSCREEN", ["F11"], ROLE.FULLSCREEN)
            ]
        };
        return view;
    }

    painter() {
        const painter = {
            label: "&Painter",
            id: MENU_IDS.PAINTER,
            submenu: [
                radio("&Select", MENU_IDS.PAINTER_SELECT, ["S"], this.onPainterSelect),
                radio("Draw &Rectangle", MENU_IDS.PAINTER_DRAW_RECT, ["R"], this.onPainterRect),
                radio("Draw &Polygon", MENU_IDS.PAINTER_DRAW_POLY, ["P"], this.onPainterPoly),
                radio("Resi&ze", MENU_IDS.PAINTER_RESIZE, ["I"], this.onPainterResize),
                radio("&Tweak", MENU_IDS.PAINTER_TWEAK, ["T"], this.onPainterTweak),
                separator(),
                normal("&Export Polygons to Clipboard", MENU_IDS.PAINTER_EXPORT, [], this.onPainterExport),
                separator(),
                normal("&Settings", MENU_IDS.PAINTER_SETTINGS, [], this.onPainterSettings)
            ]
        };
        return painter;
    }

    navigator() {
        const navigator = {
            label: "&Navigator",
            id: MENU_IDS.NAVIGATOR,
            submenu: [
                normal("&Open Spritesheet...", MENU_IDS.NAVIGATOR_OPEN, [CMD_OR_CTRL, "O", "S"], this.onNavigatorOpen),
                normal("&Close Spritesheet", MENU_IDS.NAVIGATOR_CLOSE, [], this.onNavigatorClose),
                separator(),
                normalDisabled("&Export Cells to Clipboard", MENU_IDS.NAVIGATOR_EXPORT, [], this.onNavigatorExport),
                separator(),
                normal("&Settings", MENU_IDS.NAVIGATOR_SETTINGS, [], this.onNavigatorSettings)
            ]
        };
        return navigator;
    }

    help() {
        const help = {
            label: "&Help",
            id: "HELP",
            submenu: [
                normal("&About", "HELP_ABOUT", [], this.onAbout),
                normal("&Release Notes", "HELP_RELEASE_NOTES", [], this.onReleaseNotes),
                normal("&Documentation", "HELP_DOCUMENTATION", [], this.onDocumentation),
                separator(),
                normalWithRole("Reload", "HELP_RELOAD", [CMD_OR_CTRL, SHIFT, "R"], ROLE.RELOAD),
                normalWithRole("Debug", "HELP_DEBUG", [CMD_OR_CTRL, SHIFT, "I"], ROLE.DEVTOOLS)
            ]
        };
        return help;
    }
}

function normal(label, id, accArgs, action) {
    return {
        type: ITEM_TYPES.NORMAL,
        id: id,
        label: label,
        accelerator: accArgs.join("+"),
        click: (menuItem, browserWindow, event) => action()
    };
}


function normalDisabled(label, id, accArgs, action) {
    return {
        type: ITEM_TYPES.NORMAL,
        id: id,
        label: label,
        accelerator: accArgs.join("+"),
        enabled: false,
        click: (menuItem, browserWindow, event) => action()
    };
}

function normalWithRole(label, id, accArgs, role) {
    return {
        type: ITEM_TYPES.NORMAL,
        id: id,
        role: role,
        label: label,
        accelerator: accArgs.join("+")
    };
}

function radio(label, id, accArgs, action) {
    return {
        type: ITEM_TYPES.RADIO,
        id: id,
        label: label,
        accelerator: accArgs.join("+"),
        click: (menuItem, browserWindow, event) => action()
    };
}

function separator() {
    return {
        type: ITEM_TYPES.SEPARATOR
    };
}

export function enableMenuItem(id, enabled) {
    const item = remote
        .Menu
        .getApplicationMenu()
        .getMenuItemById(id);

    item.enabled = enabled;
}

export function checkMenuItem(id, checked) {
    const item = remote
        .Menu
        .getApplicationMenu()
        .getMenuItemById(id);

    item.checked = checked;
}
