import {
    ResourcesDirName,
    AppSettingsFileName,
    NavigatorSettingsFileName,
    PainterSettingsFileName,
    TooltipsFileName
} from "./constants";

const electronRemote = require("electron").remote;
const app = electronRemote.app;

const getResourcesPath = () => {
    return `${app.getAppPath()}\\${ResourcesDirName}`;
}
export const getAppSettingsPath = () => {
    return `${getResourcesPath()}\\${AppSettingsFileName}`;
}
export const getNavigatorSettingsPath = () => {
    return `${getResourcesPath()}\\${NavigatorSettingsFileName}`;
}
export const getPainterSettingsPath = () => {
    return `${getResourcesPath()}\\${PainterSettingsFileName}`;
}
export const getTooltipsPath = () => {
    return `${getResourcesPath()}\\${TooltipsFileName}`;
}