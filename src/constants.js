export const ResourcesDirName = "resources";
export const TooltipsFileName = "tooltips.json";
export const AppSettingsFileName = "settings.json";
export const NavigatorSettingsFileName = "navigatorSettings.json";
export const PainterSettingsFileName = "painterSettings.json";

export const BOX_TYPES = {
    HIT: "Hit",
    HURT: "Hurt",
    BLOCK: "Block",
    TRIGGER: "Trigger",
    OTHER: "Other"
};
