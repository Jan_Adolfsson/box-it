import { loadTooltips } from "./tooltips";

export class TextMgr {
    constructor() {
        this.tooltips = {};
    }
    load(path) {
        this.tooltips = loadTooltips(path);
    }
    getTooltip(name) {
        const tooltip = this.tooltips[name];
        if (tooltip) {
            return tooltip;
        }

        console.log(`Text for tooltip ${name} doesn't exist.`);
        return "";
    }
}