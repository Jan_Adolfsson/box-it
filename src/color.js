export function toRGBA(hex) {
    if (hex.length === 7) {
        var r = parseInt(hex.slice(1, 3), 16),
            g = parseInt(hex.slice(3, 5), 16),
            b = parseInt(hex.slice(5, 7), 16);

        return {
            r: r,
            g: g,
            b: b,
            a: 1
        };
    } else if (hex.length === 9) {
        var r = parseInt(hex.slice(1, 3), 16),
            g = parseInt(hex.slice(3, 5), 16),
            b = parseInt(hex.slice(5, 7), 16),
            a = parseInt(hex.slice(7, 9), 16);

        return {
            r: r,
            g: g,
            b: b,
            a: a
        };
    } else {
        throw "Unhandled."
    }
};

export function toAdjustedRGBA(hex, alpha) {
    const rgba = toRGBA(hex);
    rgba.a = alpha;

    return rgba;
}

export function toRGBAString(clr) {
    return "rgba(" + clr.r + ", " + clr.g + ", " + clr.b + ", " + clr.a + ")";
};

export function adjustAlphaHEX(hex, alpha) {
    if (!(hex.length == 7 || hex.length == 9)) {
        throw error("Invalid length of HEX color value. Must be 7 or 9.");
    }

    const a = Math.floor(alpha * 255);
    const alphaHEX = a < 16 ? `0${a.toString(16)}` : a.toString(16);
    const adjustedHEX = hex.length == 7 ? `${hex}${alphaHEX}` : `${hex.substring(0, 7)}${alphaHEX}`;

    return adjustedHEX;
}