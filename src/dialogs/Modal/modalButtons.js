export const MODAL_BUTTONS = {
    OK: "ok",
    CANCEL: "cancel",
    CLOSE: "close",
    YES: "yes",
    NO: "no",
    RESET: "reset"
};
