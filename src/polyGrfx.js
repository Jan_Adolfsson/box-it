import { toRGBA, toRGBAString } from "./color";

const createBoundingBox = (poly) => {
    let x1 = poly.vertexes[0].x,
        y1 = poly.vertexes[0].y,
        x2 = poly.vertexes[0].x,
        y2 = poly.vertexes[0].y;

    poly.vertexes.forEach(vertex => {
        x1 = Math.min(x1, vertex.x);
        y1 = Math.min(y1, vertex.y);
        x2 = Math.max(x2, vertex.x);
        y2 = Math.max(y2, vertex.y);
    });

    return {
        x: x1,
        y: y1,
        w: x2 - x1,
        h: y2 - y1
    };
}


export function drawPolys(drawCtx, polys, appearance, zoom, offset) {
    if (!polys || polys.length === 0) {
        return;
    }

    const getColorByBoxType = (boxTypeConfs, defaultClr, boxType) => {
        const boxTypeConf = boxTypeConfs.find(conf => conf.type === boxType)
        let hexClr = boxTypeConf ? boxTypeConf.color : defaultClr;
        const rgba = toRGBA(hexClr);
        return rgba;
    }
    polys.forEach(poly => {

        const rgba = getColorByBoxType(appearance.boxTypes, appearance.defaultColor, poly.boxType);

        const lineWidth = poly.selected && poly.closed ? appearance.lineWidthSelected : appearance.lineWidth;
        drawPoly(
            drawCtx,
            poly,
            rgba,
            lineWidth,
            zoom,
            offset
        );

        if (appearance.showTags) {
            const bbox = createBoundingBox(poly);
            const tagSize = { w: 2, h: 1 };
            const tagOffset = { x: -4, y: 1 };
            const tagVertSpace = 1;
            if (bbox.w > tagOffset.x + tagSize.w && bbox.h > tagOffset.y + tagSize.h) {
                poly.tags.forEach((tagName, i) => {

                    const tag = appearance.tags.find(t => t.type === tagName);
                    drawCtx.ctx.fillStyle = tag ? tag.color : "#000000";

                    const xZoomed = (bbox.x + tagOffset.x) * zoom;
                    const yZoomed = ((bbox.y + tagOffset.y) + ((tagSize.h + tagVertSpace) * i)) * zoom;


                    drawCtx.ctx.fillRect(xZoomed, yZoomed, tagSize.w * zoom, tagSize.h * zoom);

                });
            }
        }

        if (appearance.showGrip) {
            poly.vertexes.forEach(vtx => {
                const lineWidth = vtx.selected ? appearance.selectedLineWidth : appearance.lineWidth;
                drawPolyGrip(drawCtx, vtx, appearance.gripSize, rgba, zoom, lineWidth, offset);
            });
        }
    });
}
export function drawPoly(drawCtx, poly, color, lineWidth, zoom, offset) {
    if (!poly.vertexes || poly.vertexes.length === 0) {
        return;
    }

    drawCtx.ctx.beginPath();
    drawCtx.ctx.strokeStyle = toRGBAString(color);
    drawCtx.ctx.lineWidth = lineWidth;

    let pos = calcPos(poly.vertexes[0], offset, zoom);
    drawCtx.ctx.moveTo(pos.x, pos.y);
    for (let i = 1; i < poly.vertexes.length; ++i) {
        pos = calcPos(poly.vertexes[i], offset, zoom);
        drawCtx.ctx.lineTo(pos.x, pos.y);
    }

    if (poly.closed) {
        drawCtx.ctx.closePath();
    }
    drawCtx.ctx.stroke();
}
function calcPos(vtx, offset, zoom) {
    return {
        x: (vtx.x + (!offset ? 0 : offset.x)) * zoom,
        y: (vtx.y + (!offset ? 0 : offset.y)) * zoom
    };
}

export function fillPoly(drawCtx, zoom, poly, appearance, gripInfo, offset) {
    if (!poly.vertexes || poly.vertexes.length === 0) {
        return;
    }


    drawCtx.ctx.beginPath();
    drawCtx.ctx.strokeStyle = toRGBAString(appearance.lineColor);
    drawCtx.ctx.setLineDash([3, 3]);
    drawCtx.ctx.lineWidth = poly.selected && poly.closed ? appearance.lineWidthSelected : appearance.lineWidth;

    let pos = calcPos(poly.vertexes[0], offset, zoom);
    drawCtx.ctx.moveTo(pos.x, pos.y);
    for (let i = 1; i < poly.vertexes.length; ++i) {
        pos = calcPos(poly.vertexes[i], offset, zoom);
        drawCtx.ctx.lineTo(pos.x, pos.y);
    }

    if (poly.closed) {
        drawCtx.ctx.closePath();
    }
    drawCtx.ctx.stroke();

    if (appearance.fillColor) {
        const region = new Path2D();
        pos = calcPos(poly.vertexes[0], offset, zoom);
        region.moveTo(pos.x, pos.y);
        pos = calcPos(poly.vertexes[1], offset, zoom);
        region.moveTo(pos.x, pos.y);
        pos = calcPos(poly.vertexes[2], offset, zoom);
        region.moveTo(pos.x, pos.y);
        pos = calcPos(poly.vertexes[3], offset, zoom);
        region.moveTo(pos.x, pos.y);
        region.closePath();

        drawCtx.ctx.fillStyle = toRGBAString(appearance.fillColor);
        drawCtx.ctx.fill();
    }

    if (gripInfo) {
        poly.vertexes.forEach(p => {
            const gripLineWidth = p.selected ? gripInfo.lineWidthSelected : gripInfo.lineWidth;
            drawPolyGrip(drawCtx, p, gripInfo.size, color, zoom, gripLineWidth, offset);
        });
    }
}

export function drawPolyGrips(drawCtx, vertexes, size, color, zoom, offset) {
    if (!vertexes || vertexes.length === 0) {
        return;
    }

    vertexes.forEach(p => drawPolyGrip(drawCtx, p, size, color, zoom, lineWidth, offset));
}

export function drawPolyGrip(drawCtx, pt, size, color, zoom, lineWidth, offset) {
    const sizeZoomed = size;
    const half = sizeZoomed / 2;

    const pos = calcPos(pt, offset, zoom);
    const x = pos.x - half;
    const y = pos.y - half;

    drawCtx.ctx.fillStyle = "white";
    drawCtx.ctx.fillRect(x, y, sizeZoomed, sizeZoomed);

    drawCtx.ctx.strokeStyle = toRGBAString(color);
    drawCtx.ctx.lineWidth = lineWidth;
    drawCtx.ctx.strokeRect(x, y, sizeZoomed, sizeZoomed);
}