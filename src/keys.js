export const Keys = {
    arrowDown: "ArrowDown",
    arrowRight: "ArrowRight",
    arrowLeft: "ArrowLeft",
    arrowUp: "ArrowUp",
    control: "Control",
    enter: "Enter",
    escape: "Escape",
    shift: "Shift",
    space: " ",
    tab: "Tab"
}

export function isArrowKey(key) {
    return key === Keys.arrowUp ||
        key === Keys.arrowDown ||
        key === Keys.arrowLeft ||
        key === Keys.arrowRight;
}