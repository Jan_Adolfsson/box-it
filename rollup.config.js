import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import { terser } from 'rollup-plugin-terser';

const production = !process.env.ROLLUP_WATCH;

export default [
	{
		input: 'src/App.js',
		output: {
			format: 'iife',
			name: 'app',
			file: 'static/bundle.js',
			sourcemap: false
		},
		plugins: [
			svelte({
				dev: !production,
				css: css => {
					css.write('static/bundle.css')
				}
			}),
			resolve(),
			commonjs(),
			production && terser()
		],
		watch: {
			clearScreen: false
		}
	}
];